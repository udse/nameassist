package nameassist.nameTemplates;

import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTNode;

public abstract class TestNameTemplate {
	
	
	public abstract String getMinimalTestName();
	
	public abstract String getSmallTestName();
			
	public abstract String getMediumTestName();
			
	public abstract String getFullTestName();		
		
	public abstract String toString();
}
