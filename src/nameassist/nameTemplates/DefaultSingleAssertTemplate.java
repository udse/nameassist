package nameassist.nameTemplates;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTNode;

public class DefaultSingleAssertTemplate extends TestNameTemplate {
	String featureUnderTest;
	String actual;
	String assertion;
	String expected;
	List<ASTNode> scenario;
	
	public DefaultSingleAssertTemplate(String featureUnderTest,String actual,String assertion,String expected,List<ASTNode> scenario) {
		this.featureUnderTest=featureUnderTest;
		this.actual=actual;
		this.assertion=assertion;
		this.expected=expected;
		this.scenario=scenario;		
	}
	
	
	public String getMinimalTestName(){
		if(null!=featureUnderTest){
			return String.format("test[%s]", featureUnderTest);
		}
		else{
			return String.format("test[%s]", actual);
		}
	}
	
	public String getSmallTestName(){
		if(null!=featureUnderTest){
			return String.format("%s_[%s][%s][%s]", getMinimalTestName(),actual,assertion,expected);
		}
		else{
			return String.format("%s[%s][%s]", getMinimalTestName(),assertion,expected);
		}
	}		
	
	
	public String getMediumTestName(){
		if(scenario.size()>0){
			return String.format("%s when [%s]",getSmallTestName(),scenario.get(0).toString());
		}
		else{
			return getSmallTestName();
		}
	}
	
	public String getFullTestName(){
		if(scenario.size()>1){
			String scenarioString=scenario.
				   	stream()
				   	.map(ASTNode::toString)
				   	.collect(Collectors.joining(" ,"));	
			return String.format("%s when [%s]",getSmallTestName(),scenarioString);
		}
		else{
			return getMediumTestName();
		}
	}


	@Override
	public String toString() {
		return getFullTestName();
	}
		
}
