package nameassist.finderFactories;

import nameassist.classifier.TestMethodClassifier;
import nameassist.classifier.TestMethodType;
import nameassist.testPredicateFinder.BeforeAndAfterFactsFinder;
import nameassist.testPredicateFinder.DefaultMultipleAssertContiguousFactsFinder;
import nameassist.testPredicateFinder.DefaultMultipleAssertSeparateFactsFinder;
import nameassist.testPredicateFinder.DefaultSingleAssertFactsFinder;
import nameassist.testPredicateFinder.ExceptionFactsFinder;
import nameassist.testPredicateFinder.FactsFinder;
import nameassist.testPredicateFinder.LoopFactsFinder;

import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;

import edu.udel.headless.action.ActionGraph;
import edu.udel.headless.action.ActionGraphBuilder;
import edu.udel.headless.cache.MethodDeclarationCache;
import edu.udel.headless.util.ASTNodeUtils;


public abstract class FactsFinderFactory {
			
	
	public static FactsFinder create(IMethod testMethod){
		MethodDeclaration methodDeclaration=MethodDeclarationCache.get(testMethod).get();
		System.out.println(methodDeclaration);
		
		Set<String> words = ASTNodeUtils.find(methodDeclaration, MethodInvocation.class)
				.filter(ASTNodeUtils::isAssertion)
				.flatMap(assertion -> ASTNodeUtils.find(assertion, Expression.class))
				.map(Expression::toString)
				.map(String::toLowerCase)
				.collect(Collectors.toSet());
		
		ActionGraph actionGraph = ActionGraphBuilder.build(testMethod,words);		
		
		switch(TestMethodClassifier.classify(methodDeclaration)) {				
			
		case DEFAULT_MULTIPLE_ASSERT_CONTIGUOUS:
			System.out.println(TestMethodType.DEFAULT_MULTIPLE_ASSERT_CONTIGUOUS);
			return new DefaultMultipleAssertContiguousFactsFinder(methodDeclaration,actionGraph);									
			
		case DEFAULT_MULTIPLE_ASSERT_SEPARATE:
			System.out.println(TestMethodType.DEFAULT_MULTIPLE_ASSERT_SEPARATE);
			return new DefaultMultipleAssertSeparateFactsFinder(methodDeclaration,actionGraph);
			
		case BEFORE_AND_AFTER:
			System.out.println(TestMethodType.BEFORE_AND_AFTER);
			return new BeforeAndAfterFactsFinder(methodDeclaration,actionGraph);
			
		case LOOP:
			System.out.println(TestMethodType.LOOP);
			return new LoopFactsFinder(methodDeclaration,actionGraph);
			
		case EXCEPTION:
			System.out.println(TestMethodType.EXCEPTION);
			return new ExceptionFactsFinder(methodDeclaration,actionGraph);
			
		default:
			System.out.println(TestMethodType.DEFAULT_SINGLE_ASSERT);
			return new DefaultSingleAssertFactsFinder(methodDeclaration,actionGraph);
		}		
	}				
}