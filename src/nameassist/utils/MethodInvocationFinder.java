package nameassist.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodInvocation;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

public class MethodInvocationFinder extends ASTVisitor {

	

	private LinkedList<MethodInvocation> list;

	public MethodInvocationFinder() {
		list = new LinkedList<MethodInvocation>();
	}

	public boolean visit(MethodInvocation invocation) {
		list.add(invocation);
		return true;
	}

	public List<MethodInvocation> methodInvocations() {		
		return list;
	}
	
	public static List<MethodInvocation> methodInvocationIn(ASTNode node) {
		MethodInvocationFinder finder = new MethodInvocationFinder();
		node.accept(finder);	
		List<MethodInvocation> list=finder.methodInvocations();
		for(int i=0;i<list.size();i++){			
			if(list.get(i).equals(node)) {				
				list.remove(i);
			}
		}
		return list;
	}
	
	public static List<MethodInvocation> allMethodInvocationIn(ASTNode node) {
		MethodInvocationFinder finder = new MethodInvocationFinder();
		node.accept(finder);			
		return finder.methodInvocations();
	}
	
	public static List<MethodInvocation> allHighLevelMethodInvocationIn(ASTNode node) {
		
		MethodInvocationFinder finder=new MethodInvocationFinder();
		node.accept(new ASTVisitor(){			
			public boolean visit(MethodInvocation invocation) {
				finder.list.add(invocation);
				return false;
			}						
		});
		return finder.methodInvocations();						
	}
}