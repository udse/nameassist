package nameassist.utils;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;

public class ArgumentFinder{
		
	static Expression expected;
	static Expression actual;

	public static void classifyArguments(MethodInvocation methodInvocation){
		if(methodInvocation.arguments().size()==1){
			actual=(Expression)methodInvocation.arguments().get(0);
			expected=null;
		}
		else if(methodInvocation.arguments().size()==2){
			expected=(Expression)methodInvocation.arguments().get(0);
			actual=(Expression)methodInvocation.arguments().get(1);
			if(actual instanceof BooleanLiteral	||
			   actual instanceof NullLiteral ||
			   actual instanceof CharacterLiteral ||
			   actual instanceof NumberLiteral ||
			   actual instanceof StringLiteral ||
			   actual instanceof ArrayCreation ||
			   actual instanceof ClassInstanceCreation ||
			   (actual instanceof SimpleName && 
			    ((SimpleName)actual).getIdentifier().toLowerCase().contains("expect")) ||
			   (expected instanceof SimpleName && 
			    ((SimpleName)expected).getIdentifier().toLowerCase().contains("actual"))
			   ){
				Expression tmp = actual;
				actual = expected;
				expected = tmp;
			}			   
		}		
		else if(methodInvocation.arguments().size()==3 && methodInvocation.getName().toString().equals("assertEquals")){
				if(methodInvocation.arguments().get(0) instanceof StringLiteral){
					actual=(Expression) methodInvocation.arguments().get(2);
					expected=(Expression) methodInvocation.arguments().get(1);
				}
				else{
					actual=(Expression) methodInvocation.arguments().get(1);
					expected=(Expression) methodInvocation.arguments().get(0);
				}
			
		}		
		else{
			System.out.println("Special Case: arguments");
		}
	}
	
	
	public static Expression expectedExpression(MethodInvocation methodInvocation){
		classifyArguments(methodInvocation);
		return expected;
	}
	
	public static Expression actualExpression(MethodInvocation methodInvocation){
		classifyArguments(methodInvocation);
		return actual;
	}
	
	
}