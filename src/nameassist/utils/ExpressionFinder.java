package nameassist.utils;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;


public class ExpressionFinder extends ASTVisitor{

	private LinkedList<ASTNode> list;

	public ExpressionFinder() {
		list = new LinkedList<ASTNode>();
	}
	
	public boolean visit(MethodInvocation expression) {	
		if(!Utils.isAssertion(expression)&&!Utils.isFailAssertion(expression))
		list.add(expression);
		return false;
	}
	
	public boolean visit(Assignment expression) {		
		list.add(expression);
		return false;
	}
	
	public boolean visit(VariableDeclarationFragment fragment){
		if(fragment.getInitializer()!=null)
			list.add(fragment);
		return false;
	}
	

	public List<ASTNode> expressions() {
		return list;
	}
	
	public static List<ASTNode> expressionsIn(ASTNode node) {		
		ExpressionFinder finder = new ExpressionFinder();
		if(node == null) return finder.expressions();		
		node.accept(finder);
		return finder.expressions();
	}		
	
}
