package nameassist.utils;

import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import com.google.common.collect.ImmutableSet;

public class TestMethodFinder extends ASTVisitor {
	
	private final ImmutableSet.Builder<MethodDeclaration> builder;
	
	public TestMethodFinder() {
		builder = ImmutableSet.builder();
	}
	
	public boolean visit(MethodDeclaration methodDeclaration) {
		if(methodDeclaration.getName().toString().startsWith("test")) {
			builder.add(methodDeclaration);
		}
		
		return true;
	}
	
	public Set<MethodDeclaration> tests(){
		return builder.build();
	}
	
	public static Set<MethodDeclaration> testsIn(ASTNode node) {
		TestMethodFinder finder = new TestMethodFinder();
		node.accept(finder);
		return finder.tests();
	}
}