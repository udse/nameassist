package nameassist.utils;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;

public class ModifiersFinder extends ASTVisitor {
	
	private SimpleName name;
	private LinkedList<Expression> list;
	
	public ModifiersFinder(SimpleName name){
		this.name=name;
		list=new LinkedList<Expression>();
	}
	
	
	
	
	public boolean visit(MethodInvocation expression) {	
		
		if(!Utils.isAssertion(expression)&&!Utils.isFailAssertion(expression))
			{
				List<SimpleName> names=SimpleNameFinder.namesIn(expression);
				for(SimpleName sn:names){
					if(sn.toString().equals(name.toString())&&!sn.equals(name)){
						list.add(expression);
						break;
					}
				}
				
			}
		
			
		return false;
	}
	
	public boolean visit(Assignment expression) {		
		List<SimpleName> names=SimpleNameFinder.namesIn(expression);
		for(SimpleName sn:names){
			if(sn.toString().equals(name.toString())&&!sn.equals(name)){
				list.add(expression);
				break;
			}
		}
		return false;
	}		
	

	public List<Expression> modifiers() {
		return list;
	}
	
	public static List<Expression> modifiersWith(ASTNode node, SimpleName name) {		
		ModifiersFinder finder = new ModifiersFinder(name);
		if(node == null) return finder.modifiers();		
		node.accept(finder);
		return finder.modifiers();
	}		
	

}
