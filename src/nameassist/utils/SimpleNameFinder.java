package nameassist.utils;

import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.SimpleName;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class SimpleNameFinder extends ASTVisitor {

	private final ImmutableList.Builder<SimpleName> builder;

	public SimpleNameFinder() {
		builder = ImmutableList.builder();
	}

	public boolean visit(SimpleName name) {		
		builder.add(name);
		return true;
	}

	public List<SimpleName> names() {
		return builder.build();
	}
	
	public static List<SimpleName> namesIn(ASTNode node) {		
		SimpleNameFinder finder = new SimpleNameFinder();
		if(node == null) return finder.names();
		node.accept(finder);
		return finder.names();
	}
}