package nameassist.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

public class VariableDeclarationFragmentFinder extends ASTVisitor {

	

	private LinkedList<VariableDeclarationFragment> list;

	public VariableDeclarationFragmentFinder() {
		list = new LinkedList<VariableDeclarationFragment>();
	}

	public boolean visit(VariableDeclarationFragment fragment) {
		if(fragment.getInitializer()!=null) list.add(fragment);
		return true;
	}

	public List<VariableDeclarationFragment> variableDeclarationFragments() {		
		return list;
	}
	
	public static List<VariableDeclarationFragment> variableDeclarationFragmentsIn(ASTNode node) {
		VariableDeclarationFragmentFinder finder = new VariableDeclarationFragmentFinder();
		node.accept(finder);	
		return finder.variableDeclarationFragments();		
	}
}