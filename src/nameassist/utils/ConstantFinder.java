package nameassist.utils;

import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;

import com.google.common.collect.ImmutableSet;

public class ConstantFinder{
	
	
	public static String stripString(String str){
		return str.replace("\"", "")
				.replace("\'", "")
				.trim();
	}
	
	
	public static String constantIn(MethodInvocation assertion) {				
		
		if(assertion.arguments().size()==1){
			return assertion.getName().toString().substring(6);
		}
		else{
			Expression expression=ArgumentFinder.expectedExpression(assertion);
			if(Utils.isConstant(expression)){
						return stripString(expression.toString());
					}
					else{
						return "";
					}
					
		}	
	}
}