package nameassist.utils;

import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodInvocation;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class AssertionFinder extends ASTVisitor {

	

	private final ImmutableList.Builder<MethodInvocation> builder;

	public AssertionFinder() {
		builder = ImmutableList.builder();
	}

	public boolean visit(MethodInvocation invocation) {
		if (Utils.isAssertion(invocation)) {
			builder.add(invocation);
		}
		return true;
	}

	public List<MethodInvocation> asserts() {
		return builder.build();
	}
	
	public static List<MethodInvocation> assertIn(ASTNode node) {
		AssertionFinder finder = new AssertionFinder();
		node.accept(finder);
		return finder.asserts();
	}
}