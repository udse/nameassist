package nameassist.utils;

import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.StringLiteral;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;

public class Utils {
	
	static Set<String> assertMethodNames = ImmutableSet.of(
			"assertTrue",
			"assertFalse", 
			"assertEquals",
			"assertSame",
			"assertNull",
			"assertNotNull",
			"assertListEquals"
			);
	
	public static boolean isAssertion(MethodInvocation invocation){		
			return assertMethodNames.contains(invocation.getName().toString());
		}
	
	public static boolean isFailAssertion(MethodInvocation invocation){		
		return invocation.getName().toString().equals("fail");
	}
	
	public static boolean isConstant(ASTNode node){
			if(node instanceof BooleanLiteral||
				   node instanceof NullLiteral||
				   node instanceof CharacterLiteral ||
				   node instanceof NumberLiteral ||
				   node instanceof StringLiteral){
				return true;
			}
			return false;
	}


}
