package nameassist.testPredicateFinder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import nameassist.nameTemplates.TestNameTemplate;
import nameassist.testPredicate.DefaultSingleAssertPredicate;
import nameassist.testPredicate.TestPredicate;
import nameassist.utils.ArgumentFinder;
import nameassist.utils.MethodInvocationFinder;
import nameassist.utils.ModifiersFinder;
import nameassist.utils.SimpleNameFinder;
import nameassist.utils.Utils;
import nameassist.utils.VariableDeclarationFragmentFinder;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import edu.udel.headless.action.ActionGraph;

import org.eclipse.jdt.core.dom.InfixExpression.Operator;

public abstract class FactsFinder {
	
	protected MethodDeclaration test;
	protected TestPredicate predicate;
	protected ActionGraph actionGraph;
	
	
	public FactsFinder(MethodDeclaration methodDeclaration,ActionGraph actionGraph){
		this.test=methodDeclaration;
		this.actionGraph=actionGraph;
		findPredicate();
		findVariableTypeTable();
	}
	
	public ActionGraph getActionGraph(){
		return this.actionGraph;
	}
	
	private void findVariableTypeTable() {
		HashMap<String,String> map=new HashMap<String,String>();
		List<VariableDeclarationFragment> fragments=VariableDeclarationFragmentFinder.variableDeclarationFragmentsIn(test);		
		for(VariableDeclarationFragment fragment:fragments){
			if(!(fragment.getParent() instanceof VariableDeclarationStatement)) continue;
			VariableDeclarationStatement statement=(VariableDeclarationStatement)fragment.getParent();
			map.put(fragment.getName().toString(), statement.getType().toString());
		}
		
		ASTNode node=test;
		while(!(node instanceof TypeDeclaration)){
			node=node.getParent();
		}
		
		TypeDeclaration typeDeclaration=(TypeDeclaration)node;
		List<FieldDeclaration> fieldDeclarations=new LinkedList<FieldDeclaration>();
		
		typeDeclaration.accept(new ASTVisitor(){
			public boolean visit(FieldDeclaration declaration) {				
					fieldDeclarations.add(declaration);				
				return false;
			}		
		});
		
		for(FieldDeclaration d:fieldDeclarations){
			List<VariableDeclarationFragment> fs=d.fragments();
			for(VariableDeclarationFragment f:fs){
				map.put(f.getName().toString(),d.getType().toString());
			}
		}
		predicate.setVariableTable(map);					
	}

	protected abstract void findPredicate();
	
	
	public String toString(){
		return predicate.toString();
	}
	
	public TestNameTemplate generateTemplate(){
		return predicate.getTemplate(actionGraph);
	}
	
		
	protected List<ASTNode> getAssignmentsAndVariableDeclarations(){
		LinkedList<ASTNode> list=new LinkedList<ASTNode>();
		List<Statement> statements=test.getBody().statements();
		for(Statement statement:statements){
			if(statement instanceof ExpressionStatement){
				Expression exp=((ExpressionStatement)statement).getExpression();				
				if(exp instanceof Assignment){
					list.add(exp);
				}
			}			
			if(statement instanceof VariableDeclarationStatement){
				List<VariableDeclarationFragment> fragments=VariableDeclarationFragmentFinder.variableDeclarationFragmentsIn(statement);				
				list.addAll(fragments);				
			}
		}
	   return list;
	}
	
	protected boolean simpleNameOnLeftSide(SimpleName name, ASTNode exp){
		
		
		if(exp instanceof Assignment){
			Assignment assignment=(Assignment)exp;
			Expression leftSide=assignment.getLeftHandSide();
			if(leftSide instanceof SimpleName &&((SimpleName)leftSide).getIdentifier().equals(name.getIdentifier())){						
			  return true;													
			}
		} else if(exp instanceof VariableDeclarationFragment){
			VariableDeclarationFragment variDec=(VariableDeclarationFragment)exp;						
			String identifier=variDec.getName().getIdentifier();
			if(identifier.equals(name.getIdentifier())) return true;								
		} else if(exp instanceof VariableDeclarationExpression){
			exp=(ASTNode) ((VariableDeclarationExpression)exp).fragments().get(0);			
			VariableDeclarationFragment variDec=(VariableDeclarationFragment)exp;						
			String identifier=variDec.getName().getIdentifier();
			if(identifier.equals(name.getIdentifier())) return true;
		}
		
		return false;		
	}
	
	
protected ASTNode findConstant(Expression expr){
	
		if(Utils.isConstant(expr)) return expr;
		
		if(expr instanceof SimpleName){
			List<ASTNode> expressions=getAssignmentsAndVariableDeclarations();
			for(int i=expressions.size()-1;i>=0;i--){
				ASTNode exp=expressions.get(i);			
				if(!simpleNameOnLeftSide((SimpleName)expr, exp)) continue;
				if(exp instanceof Assignment){
					Assignment assignment=(Assignment)exp;
					if(Utils.isConstant(assignment.getRightHandSide()))
							return assignment.getRightHandSide();
					
				} else if(exp instanceof VariableDeclarationFragment){
					VariableDeclarationFragment variDec=(VariableDeclarationFragment)exp;						
					if(Utils.isConstant(variDec.getInitializer()))
						return variDec.getInitializer();								
				}								
			}										
		}
		return null;
	}


protected Expression trackVariableDefinition(List<ASTNode> expressions, SimpleName name){				
		
		for(int i=expressions.size()-1;i>=0;i--){
			ASTNode exp=expressions.get(i);	
			if(!simpleNameOnLeftSide(name, exp)) continue;
			if(exp instanceof Assignment){
				Assignment assignment=(Assignment)exp;				
				return assignment.getRightHandSide();
				
			} else if(exp instanceof VariableDeclarationFragment){
				VariableDeclarationFragment variDec=(VariableDeclarationFragment)exp;						
				return variDec.getInitializer();								
			}								
		}											
	return null;
}

	protected boolean isLocalMethod(MethodInvocation invocation){
	if(invocation.getExpression()==null) return true;
	else return false;
	}
	
	protected MethodInvocation findMethodInvocation(ASTNode node){
		List<MethodInvocation> list=MethodInvocationFinder.methodInvocationIn(node);
		if(list.size()>0) return list.get(0);
		List<SimpleName> names=SimpleNameFinder.namesIn(node);
		List<ASTNode> expressions=getAssignmentsAndVariableDeclarations();
		
		Set<String> visited=new HashSet<String>();
		LinkedList<SimpleName> worklist=new LinkedList<SimpleName>();
		worklist.addAll(names);
		
		while(!worklist.isEmpty()){
			SimpleName name=worklist.removeFirst();
			for(int i=expressions.size()-1;i>=0;i--){
				ASTNode exp=expressions.get(i);	
				// System.out.println("TEST: "+name+" : "+exp+" : "+simpleNameOnLeftSide(name, exp));
				if(!simpleNameOnLeftSide(name, exp)) continue;
				visited.add(name.getIdentifier());
				list=MethodInvocationFinder.methodInvocationIn(exp);
				if(list.size()>0) return list.get(0);
				names=SimpleNameFinder.namesIn(exp);
				for(SimpleName simpleName:names){
					if(visited.contains(simpleName.getIdentifier())) continue;
					worklist.add(simpleName);
				}								
			}
		}	   
		return null;
	}
	
	
	protected Operator reversedOperator(Operator op){
		if(op.equals(Operator.EQUALS)){
			  return Operator.NOT_EQUALS;
		  } 
		  if(op.equals(Operator.LESS)){
			  return Operator.GREATER_EQUALS;
		  } 
		  if(op.equals(Operator.GREATER)){
			  return Operator.LESS_EQUALS;
		  }
		  if(op.equals(Operator.LESS_EQUALS)){
			  return Operator.GREATER;
		  }
		  if(op.equals(Operator.GREATER_EQUALS)){
			  return Operator.LESS;
		  }
		  if(op.equals(Operator.NOT_EQUALS)){
			  return Operator.EQUALS;
		  }
		  return null;
	}
	
	protected String translateOperator(Operator op){
	  if(op==null){
		  return "UNKNOWN_OP";
	  }
	  if(op.equals(Operator.EQUALS)){
		  return "is equal to";
	  }
	  if(op.equals(Operator.LESS)){
		  return "is less than";
	  }
	  if(op.equals(Operator.GREATER)){
		  return "is greater than";
	  }
	  if(op.equals(Operator.LESS_EQUALS)){
		  return "is not greater than";
	  }
	  if(op.equals(Operator.GREATER_EQUALS)){
		  return "is not less than";
	  }
	  if(op.equals(Operator.NOT_EQUALS)){
		  return "is not equal to";
	  }
	  return "UNKNOWN_OP";
	  	  
	}
		
	
	protected DefaultSingleAssertPredicate oneArgumentAssertExpectedOutcome(MethodInvocation assertion){
		DefaultSingleAssertPredicate predicate=new DefaultSingleAssertPredicate();
		if(assertion.arguments().size()!=1){
			System.out.println("Error!");
			return null;
		}		
		predicate.assertion=null;
		
		Expression expr=(Expression)assertion.arguments().get(0);
		
		if(expr instanceof InstanceofExpression){
			if(assertion.getName().toString().equals("assertTrue")){
				predicate.predicateString="is an instance of";
			}
			else if(assertion.getName().toString().equals("assertFalse")){
				predicate.predicateString="is not an instance of";
			}
			else{
				predicate.predicateString="UNKNOWN";
			}													
			predicate.expectedValueObject=((InstanceofExpression) expr).getRightOperand();		
		}
		else if(expr instanceof InfixExpression){
			 Operator operator=((InfixExpression)expr).getOperator();
			 Expression leftOperand=((InfixExpression)expr).getLeftOperand();
			 Expression rightOperand=((InfixExpression)expr).getRightOperand();		 
			 ASTNode constant=findConstant(leftOperand);
			 if(constant!=null){
				 operator=reversedOperator(operator);
			 }
			 else{
				 constant=findConstant(rightOperand);				 
			 }
			 
			 if(assertion.getName().toString().equals("assertFalse")){
				 operator=reversedOperator(operator);
			 }
			 
			 predicate.predicateString=translateOperator(operator);
			 predicate.expectedValueObject=constant;			 			 
		} else if(expr instanceof MethodInvocation && isLocalMethod((MethodInvocation)expr)){
			 MethodInvocation invocation=(MethodInvocation)expr;
			predicate.predicateString=invocation.getName().toString();
			List<Expression> arguments=invocation.arguments();
			for(Expression exp:arguments){
				if(exp instanceof MethodInvocation) continue;
				if(findMethodInvocation(exp)!=null) continue;
				ASTNode constant=findConstant(exp);
				if(constant!=null) predicate.expectedValueObject=constant;
				else predicate.expectedValueObject=exp;
				break;				
			}
		} else if(assertion.getName().toString().equals("assertNull")){
			predicate.predicateString="is";
			predicate.expectedValueObject=null;
		}else if(assertion.getName().toString().equals("assertNotNull")){
			predicate.predicateString="is not";
			predicate.expectedValueObject=null;
		}			
		else 
		{		
				predicate.assertion=assertion.getName();
				System.out.println("Special case : one argument assertion!");
		}
		return predicate;
	}
	
	
protected DefaultSingleAssertPredicate getPredicateFactsFromAssertion(MethodInvocation assertion){
		
		DefaultSingleAssertPredicate predicate;
		Expression expectedExpression = ArgumentFinder.expectedExpression(assertion);
		if(expectedExpression==null){
			predicate=oneArgumentAssertExpectedOutcome(assertion);			
		}
		else{
			
			predicate=new DefaultSingleAssertPredicate();
			predicate.assertion=assertion.getName();														
			
			ASTNode constant=findConstant(expectedExpression);
			if(constant!=null){
				predicate.expectedValueObject=constant;
			}
			else{
				predicate.expectedValueObject=expectedExpression;
			}
		}
		return predicate;
	}

	protected List<Expression> findActionModifiers(SimpleName acitonObject){
		return ModifiersFinder.modifiersWith(test,acitonObject);		
	}
			
}
