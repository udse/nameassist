package nameassist.testPredicateFinder;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;

import edu.udel.headless.action.ActionGraph;
import nameassist.ASTVisitors.exceptionPattern.AssertionsInCatchBlockFinder;
import nameassist.ASTVisitors.exceptionPattern.CatchedExceptionTypeFinder;
import nameassist.ASTVisitors.exceptionPattern.ExceptionScenarioUnderTestFinder;
import nameassist.testPredicate.ExceptionPredicate;
import nameassist.utils.Utils;

public class ExceptionFactsFinder extends FactsFinder{

	public ExceptionFactsFinder(MethodDeclaration methodDeclaration,ActionGraph actionGraph) {
		super(methodDeclaration,actionGraph);		
	}

	@Override
	protected void findPredicate() {
		ExceptionPredicate predicate=new ExceptionPredicate();
		
		ExceptionScenarioUnderTestFinder scenarioFinder=new ExceptionScenarioUnderTestFinder();
		test.accept(scenarioFinder);		
		List<ASTNode> nodes=scenarioFinder.nonAssertionInTryBlock();
		for(int i=nodes.size()-1;i>=0;i--){
			ASTNode node=nodes.get(i);
			if(node instanceof MethodInvocation){
				MethodInvocation methodInvocation=(MethodInvocation)node;
				predicate.actionMethodName=methodInvocation.getName().toString();
				predicate.actionObject=methodInvocation.getExpression();
				predicate.actionModifiers.addAll(methodInvocation.arguments());
				if(methodInvocation.getExpression() instanceof SimpleName){
					predicate.actionModifiers.addAll(findActionModifiers((SimpleName)methodInvocation.getExpression()));
				}
				break;
			}
		}
		
		if(predicate.actionMethodName==null){
			for(int i=nodes.size()-1;i>=0;i--){
				ASTNode node=nodes.get(i);
				if(node instanceof ClassInstanceCreation){
					ClassInstanceCreation instanceCreation=(ClassInstanceCreation)node;
					predicate.actionMethodName="new";
				    predicate.actionObject=instanceCreation.getType();
				    predicate.actionModifiers.addAll(instanceCreation.arguments());
				    break;
			}
			}
		}
		
		AssertionsInCatchBlockFinder finder=new AssertionsInCatchBlockFinder();
		test.accept(finder);
		List<ASTNode> assertions=finder.assertionsInCatchBlock();
		
		if(assertions.size()!=0&&Utils.isFailAssertion((MethodInvocation)assertions.get(0))){
				predicate.throwException=false;			
		}
		else{
			    predicate.throwException=true;
							
			}
		predicate.assertionsInCatchBlock=assertions;
		CatchedExceptionTypeFinder exceptionTypefinder=new CatchedExceptionTypeFinder();
		test.accept(exceptionTypefinder);
		predicate.exceptionType=exceptionTypefinder.exceptionType();				
		super.predicate=predicate;		
	}
}
