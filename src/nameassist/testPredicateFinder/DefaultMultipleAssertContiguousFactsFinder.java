package nameassist.testPredicateFinder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;

import edu.udel.headless.action.ActionGraph;
import nameassist.testPredicate.DefaultMultipleAssertContiguousPredicate;
import nameassist.testPredicate.DefaultSingleAssertPredicate;
import nameassist.utils.ArgumentFinder;
import nameassist.utils.AssertionFinder;
import nameassist.utils.MethodInvocationFinder;
import nameassist.utils.SimpleNameFinder;

public class DefaultMultipleAssertContiguousFactsFinder extends DefaultSingleAssertFactsFinder {

	public DefaultMultipleAssertContiguousFactsFinder(MethodDeclaration methodDeclaration,ActionGraph actionGraph) {
		super(methodDeclaration,actionGraph);
	}

	@Override
	protected void findPredicate() {
		// find all actual parameters of assert
		// calculate the intersection
		DefaultMultipleAssertContiguousPredicate predicate=new DefaultMultipleAssertContiguousPredicate();
		
		List<MethodInvocation> asserts = AssertionFinder.assertIn(test);		
				
		List<List<MethodInvocation>> invocationList=asserts.stream()
						.map(ArgumentFinder::actualExpression)
						.map(MethodInvocationFinder::allMethodInvocationIn)
						.collect(Collectors.toList());
		
		Set<String> allHighLevelMethods=asserts.stream()
				.map(ArgumentFinder::actualExpression)
				.map(MethodInvocationFinder::allHighLevelMethodInvocationIn)			
				.flatMap(l->l.stream())
				.map(MethodInvocation::toString)
				.collect(Collectors.toSet());
		
				
				HashMap<String,MethodInvocation> map=new HashMap<String,MethodInvocation>();
				HashSet<String> set=new HashSet<String>();
				set.addAll(invocationList.get(0).stream().map(MethodInvocation::toString).collect(Collectors.toSet()));		
				for(List<MethodInvocation> list:invocationList){
					set.retainAll(list.stream().map(MethodInvocation::toString).collect(Collectors.toSet()));
					for(MethodInvocation invocation:list){						
						map.put(invocation.toString(), invocation);
					}
				}
				
				if(set.size()>0){
					for(String str:set){
						MethodInvocation invo=map.get(str);						
						predicate.actualValueObject=invo;
						predicate.actualValueModifiers.addAll(invo.arguments());
						if(invo.getExpression() instanceof SimpleName){
							predicate.actualValueModifiers.addAll(findActionModifiers((SimpleName)invo.getExpression()));
						}
						
						for(String s:allHighLevelMethods){
							if(s.startsWith(invo.toString())&&!s.equals(invo.toString())){								
								predicate.actualValueMethodNames.add(map.get(s).getName().toString());
							}							
						}	
						
						break;
					}
				}
				
				
				if(predicate.actualValueMethodNames.isEmpty()){
				
				List<List<SimpleName>> simpleNameList = asserts.stream()
				.map(ArgumentFinder::actualExpression)
				.map(SimpleNameFinder::namesIn)				
				.collect(Collectors.toList());
				
				HashMap<String,SimpleName> simpleNameMap=new HashMap<String,SimpleName>();
				HashSet<String> simpleNameSet=new HashSet<String>();
				simpleNameSet.addAll(simpleNameList.get(0).stream().map(SimpleName::toString).collect(Collectors.toSet()));		
				for(List<SimpleName> list:simpleNameList){
					simpleNameSet.retainAll(list.stream().map(SimpleName::toString).collect(Collectors.toSet()));
					for(SimpleName name:list){
						simpleNameMap.put(name.toString(), name);
					}
				}			
								
				if(simpleNameSet.size()>0){
					for(String str:simpleNameSet){						
						SimpleName name=simpleNameMap.get(str);						
						predicate.actualValueObject=name;						
						predicate.actualValueModifiers.addAll(findActionModifiers(name));						
						for(String s:allHighLevelMethods){							
							if(s.startsWith(name.toString())){
								predicate.actualValueMethodNames.add(map.get(s).getName().toString());
								
							}							
						}						
						break;
					}
				}
				
				if(predicate.actualValueMethodNames.isEmpty()){		
					Expression actualExpression=ArgumentFinder.actualExpression(asserts.get(0));
					findPredicate(actualExpression,asserts.get(0));
					return;
					}
				}
				
				
				DefaultSingleAssertPredicate p=getPredicateFactsFromAssertion(asserts.get(0));
				
				for(int i=1;i<asserts.size();i++){
					DefaultSingleAssertPredicate anotherFacts=getPredicateFactsFromAssertion(asserts.get(i));
					if(!p.hasSamePredicateFacts(anotherFacts)) {
						p=null;
						break;
					}
				}
				
				if(p!=null){
					predicate.predicateString=p.predicateString;
					predicate.expectedValueObject=p.expectedValueObject;
					predicate.assertion=p.assertion;					
				}else{										
					predicate.predicateString="work correctly";
				}								
				super.predicate=predicate;
			}		
}
