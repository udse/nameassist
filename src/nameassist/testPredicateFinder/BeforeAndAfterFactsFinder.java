package nameassist.testPredicateFinder;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;

import edu.udel.headless.action.ActionGraph;
import nameassist.classifier.TestMethodClassifier;
import nameassist.testPredicate.BeforeAndAfterPredicate;
import nameassist.testPredicate.DefaultSingleAssertPredicate;
import nameassist.utils.ArgumentFinder;
import nameassist.utils.Utils;

public class BeforeAndAfterFactsFinder extends FactsFinder {

	public BeforeAndAfterFactsFinder(MethodDeclaration methodDeclaration,ActionGraph actionGraph) {
		super(methodDeclaration,actionGraph);
		
	}
	
	private void findActionFactsFromActualExpression(Expression actualExpression, BeforeAndAfterPredicate predicate){
		MethodInvocation invocation;
		if(actualExpression instanceof MethodInvocation &&(!isLocalMethod((MethodInvocation)actualExpression))){
		   invocation=(MethodInvocation)actualExpression;
                        								
		} else{						
		   invocation=findMethodInvocation(actualExpression);			
		}
		
		if(invocation!=null){
			predicate.assertionFacts.actualValueMethodName=invocation.getName().toString();
			predicate.assertionFacts.actualValueObject=invocation.getExpression();
			predicate.assertionFacts.actualValueModifiers.addAll(invocation.arguments());			
		}		
	}
	
	
	
	

	@Override
	protected void findPredicate() {
		BeforeAndAfterPredicate predicate=new BeforeAndAfterPredicate();
		
		List<MethodInvocation> seq=TestMethodClassifier.getMethodInvocations(test);
		int index=0;
		for(int i=1;i<seq.size()-1;i++){
			if(Utils.isAssertion(seq.get(i-1))&&(!Utils.isAssertion(seq.get(i)))&&Utils.isAssertion(seq.get(i+1))){
				index=i;
				break;									
			}
		}	
		MethodInvocation actionMethod=seq.get(index);
		
		predicate.middleActionMethodName=actionMethod.getName().toString();
		predicate.middleActionObject=actionMethod.getExpression();
		predicate.middleActionModifiers.addAll(actionMethod.arguments());	
		if(actionMethod.getExpression() instanceof SimpleName){
			//predicate.middleActionModifiers.addAll(findActionModifiers((SimpleName)actionMethod.getExpression()));
		}
		
				
		List<MethodInvocation> asserts=new LinkedList<MethodInvocation>();
		
		asserts.add(seq.get(index-1));
		asserts.add(seq.get(index+1));
		boolean sameAssert=seq.get(index-1).getName().toString().equals(seq.get(index+1).getName().toString());
		
		Expression beforeActual=ArgumentFinder.actualExpression(seq.get(index-1));
		Expression afterActual=ArgumentFinder.actualExpression(seq.get(index+1));
		boolean sameActualExpression=beforeActual.toString().equals(afterActual.toString());
		
		
			
		
		
		Expression beforeExpected=ArgumentFinder.expectedExpression(seq.get(index-1));
		Expression afterExpected=ArgumentFinder.expectedExpression(seq.get(index+1));
		boolean sameExpectedExpression;
		
		if(beforeExpected == null && afterExpected == null){
			sameExpectedExpression=true;			
		}
		else if(beforeExpected != null && afterExpected != null){
			sameExpectedExpression= beforeExpected.toString().equals(afterExpected.toString());
		}
		else{
			sameExpectedExpression=false;
		}
		
		findActionFactsFromActualExpression(afterActual,predicate);			
		DefaultSingleAssertPredicate predicateFacts=getPredicateFactsFromAssertion(seq.get(index+1));
		predicate.assertionFacts.predicateString=predicateFacts.predicateString;
		predicate.assertionFacts.expectedValueObject=predicateFacts.expectedValueObject;
		predicate.assertionFacts.expectedValueModifiers=predicateFacts.expectedValueModifiers;
		predicate.assertionFacts.assertion=predicateFacts.assertion;						
		
		if(sameAssert&&sameActualExpression&&sameExpectedExpression){
			predicate.change=false;						
		}
		else if(sameAssert&&sameActualExpression)
		{
			predicate.change=true;						
		}
		else {
			super.predicate=predicate.assertionFacts;
			return;
		}
		super.predicate=predicate;		
	}						

}
