package nameassist.testPredicateFinder;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;

import edu.udel.headless.action.ActionGraph;
import nameassist.ASTVisitors.LoopPattern.IteratorFinder;
import nameassist.ASTVisitors.LoopPattern.LoopAssertionFinder;
import nameassist.ASTVisitors.LoopPattern.LoopVariableFinder;
import nameassist.testPredicate.LoopPredicate;
import nameassist.utils.ArgumentFinder;
import nameassist.utils.MethodInvocationFinder;
import nameassist.utils.SimpleNameFinder;

public class LoopFactsFinder extends FactsFinder{

	public LoopFactsFinder(MethodDeclaration methodDeclaration,ActionGraph actionGraph) {
		super(methodDeclaration,actionGraph);
	}
	
	
	private void findActionFactsFromActualExpression(Expression actualExpression, LoopPredicate facts){
		MethodInvocation invocation;
		if(actualExpression instanceof MethodInvocation &&(!isLocalMethod((MethodInvocation)actualExpression))){
		   invocation=(MethodInvocation)actualExpression;
                        								
		} else{						
		   invocation=findMethodInvocation(actualExpression);			
		}
		
		if(invocation!=null){
			facts.actualValueMethodName=invocation.getName().toString();
			facts.actualValueObject=invocation.getExpression();
			facts.actualValueModifiers.addAll(invocation.arguments());
			if(invocation.getExpression() instanceof SimpleName){
				facts.actualValueModifiers.addAll(findActionModifiers((SimpleName)invocation.getExpression()));
			}
		}		
	}
	
	
	private void findActionFacts(LoopPredicate facts){
		LoopAssertionFinder loopAssertionFinder=new LoopAssertionFinder();
		test.accept(loopAssertionFinder);
		List<MethodInvocation> assertions=loopAssertionFinder.assertionsInLoop();
		Statement loopStatement=loopAssertionFinder.getLoopStatement();
		
		LoopVariableFinder loopVariableFinder=new LoopVariableFinder();
		loopStatement.accept(loopVariableFinder);		
		SimpleName loopVariable=loopVariableFinder.getLoopVariable();						
		
		Expression actualExpression=null;
		if(loopVariable!=null){			
			for(MethodInvocation assertion:assertions){
				List<Expression> arguments=assertion.arguments();
				for(int i=arguments.size()-1;i>=0;i--){	
					Expression argument=arguments.get(i);
					 if(!(argument instanceof MethodInvocation)) continue;
						List<SimpleName> names=SimpleNameFinder.namesIn(argument);
						for(SimpleName name:names){
							if(name.toString().equals(loopVariable.toString())){
								actualExpression=argument;
								break;
							}
						}
						if(actualExpression!=null) break;
					}
					if(actualExpression!=null) break;
				}						
		}
		else{
			IteratorFinder iteratorFinder=new IteratorFinder();
			loopStatement.accept(iteratorFinder);
			Expression iterator=iteratorFinder.getIterator();
			Expression iterateItem=iteratorFinder.getIterateItem();			
			
			if(iterator instanceof SimpleName){
				List<ASTNode> expressions=getAssignmentsAndVariableDeclarations();
				if(loopStatement instanceof ForStatement){
					ForStatement forStatement=(ForStatement)loopStatement;
					List<ASTNode> list=forStatement.initializers();					
					for(ASTNode node:list){
					   if(node instanceof VariableDeclarationExpression){
						   expressions.addAll(((VariableDeclarationExpression)node).fragments());
						}												
					}										
					}
			  Expression iteratorDef=trackVariableDefinition(expressions,(SimpleName)iterator);			  
			  if(iteratorDef instanceof MethodInvocation){
				  facts.actualValueObject=((MethodInvocation)iteratorDef).getExpression();
				  if(((MethodInvocation)iteratorDef).getExpression() instanceof SimpleName){
						facts.actualValueModifiers.addAll(findActionModifiers((SimpleName)((MethodInvocation)iteratorDef).getExpression()));
					}
			  }
			  else if(iteratorDef instanceof ClassInstanceCreation){
				  facts.actualValueObject=((ClassInstanceCreation)iteratorDef).getType();
			  }			  			  
			  
			  for(MethodInvocation assertion:assertions){
					List<Expression> arguments=assertion.arguments();
					for(int i=arguments.size()-1;i>=0;i--){	
						Expression argument=arguments.get(i);
						if(!(argument instanceof MethodInvocation)){
							List<MethodInvocation> list=MethodInvocationFinder.methodInvocationIn(argument);
							if(list.size()==0) break;
							argument=list.get(0);
						}
							List<SimpleName> names=SimpleNameFinder.namesIn(argument);
							for(SimpleName name:names){											
								if(name.toString().equals(iterator.toString())||
								   (iterateItem!=null && name.toString().equals(iterateItem.toString()))		
										){
									facts.actualValueMethodName=((MethodInvocation)argument).getName().toString();
									facts.actualValueModifiers.addAll(((MethodInvocation)argument).arguments());
									
									break;
								}
							}
							if(facts.actualValueMethodName!=null) break;																												
						}
						if(facts.actualValueMethodName!=null) break;
					}
			  if(facts.actualValueMethodName!=null) return;			  			  			 
			}			
		}
		
		if(actualExpression==null){
			actualExpression=ArgumentFinder.actualExpression(assertions.get(0));
			findActionFactsFromActualExpression(actualExpression, facts);
		}				
	}
	
	
	private void findPredicateFacts(LoopPredicate facts) {
		LoopAssertionFinder loopAssertionFinder=new LoopAssertionFinder();
		test.accept(loopAssertionFinder);
		List<MethodInvocation> assertions=loopAssertionFinder.assertionsInLoop();
		Expression expectedExpression=ArgumentFinder.expectedExpression(assertions.get(0));
		MethodInvocation assertInvocation=assertions.get(0);
		
		facts.assertion=assertInvocation.getName();
											
		
		ASTNode constant=findConstant(expectedExpression);
		if(constant!=null){
			facts.expectedValueObject=constant;
		}
		else{
			facts.expectedValueObject=expectedExpression;
		}		
	}

	

	@Override
	protected void findPredicate() {
		LoopPredicate facts=new LoopPredicate();
		findActionFacts(facts);
		findPredicateFacts(facts);
		super.predicate=facts;		
	}	
}
