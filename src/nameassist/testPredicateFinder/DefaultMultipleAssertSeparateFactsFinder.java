package nameassist.testPredicateFinder;

import java.util.List;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;

import edu.udel.headless.action.ActionGraph;
import nameassist.utils.ArgumentFinder;
import nameassist.utils.AssertionFinder;

public class DefaultMultipleAssertSeparateFactsFinder extends DefaultSingleAssertFactsFinder {

	public DefaultMultipleAssertSeparateFactsFinder(MethodDeclaration methodDeclaration,ActionGraph actionGraph) {
		super(methodDeclaration,actionGraph);		
	}
	
	
	@Override
	protected void findPredicate(){		
		List<MethodInvocation> assertsList = AssertionFinder.assertIn(test);
		Expression actualExpression=ArgumentFinder.actualExpression(assertsList.get(assertsList.size()-1));
		findPredicate(actualExpression,assertsList.get(assertsList.size()-1));
	}

}
