package nameassist.testPredicateFinder;

import java.util.List;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;

import edu.udel.headless.action.ActionGraph;
import nameassist.testPredicate.DefaultSingleAssertPredicate;
import nameassist.utils.ArgumentFinder;
import nameassist.utils.AssertionFinder;
import nameassist.utils.MethodInvocationFinder;
import nameassist.utils.Utils;

public class DefaultSingleAssertFactsFinder extends FactsFinder {

	public DefaultSingleAssertFactsFinder(MethodDeclaration methodDeclaration,ActionGraph actionGraph) {
		super(methodDeclaration,actionGraph);		
	}

	@Override
	protected void findPredicate(){		
		List<MethodInvocation> assertsList = AssertionFinder.assertIn(test);
		Expression actualExpression=ArgumentFinder.actualExpression(assertsList.get(0));
		findPredicate(actualExpression,assertsList.get(0));
	}
	
	
	protected void findPredicate(Expression actualExpression,MethodInvocation assertion) {
		DefaultSingleAssertPredicate predicate=new DefaultSingleAssertPredicate();		
		if(actualExpression instanceof ClassInstanceCreation){
			predicate.actualValueMethodName="new";
			ClassInstanceCreation cic=(ClassInstanceCreation)actualExpression;
			predicate.actualValueObject=cic.getType();
			predicate.actualValueModifiers.addAll(cic.arguments());			
		}
						
		MethodInvocation invocation;
		if(actualExpression instanceof MethodInvocation &&(!isLocalMethod((MethodInvocation)actualExpression))){
		   invocation=(MethodInvocation)actualExpression;
                        								
		} else{						
		   invocation=findMethodInvocation(actualExpression);			
		}
		
		if(invocation!=null){
			predicate.actualValueMethod=(IMethod)((IMethodBinding)invocation.resolveMethodBinding()).getJavaElement();
			predicate.actualValueMethodName=invocation.getName().toString();
			predicate.actualValueObject=invocation.getExpression();
			predicate.actualValueModifiers.addAll(invocation.arguments());
			if(invocation.getExpression() instanceof SimpleName){
				//predicate.actualValueModifiers.addAll(findActionModifiers((SimpleName)invocation.getExpression()));
			}
		}
		else if(actualExpression instanceof SimpleName){
			List<ASTNode> exprs=getAssignmentsAndVariableDeclarations();
			Expression expr=trackVariableDefinition(exprs, (SimpleName)actualExpression);
			if(expr instanceof ClassInstanceCreation){
				predicate.actualValueMethodName="new";
				predicate.actualValueObject=((ClassInstanceCreation) expr).getType();
				predicate.actualValueModifiers.addAll(((ClassInstanceCreation) expr).arguments());
			}
		}
		
		if(predicate.actualValueMethodName==null){			
			List<MethodInvocation> methodInvocations=MethodInvocationFinder.allHighLevelMethodInvocationIn(test);
			MethodInvocation method=null;
			for(int i=methodInvocations.size()-1;i>=0;i--){
				method=methodInvocations.get(i);				
				if(!Utils.isAssertion(method)) break;
				else method=null;
			}
			if(method!=null){
				predicate.actualValueMethod=(IMethod)((IMethodBinding)method.resolveMethodBinding()).getJavaElement();
				predicate.actualValueMethodName=method.getName().toString();
				predicate.actualValueObject=method.getExpression();
				predicate.actualValueModifiers.addAll(method.arguments());
				if(method.getExpression() instanceof SimpleName){
					//predicate.actualValueModifiers.addAll(findActionModifiers((SimpleName)method.getExpression()));
				}
			}					
		}
								
		Expression expectedExpression = ArgumentFinder.expectedExpression(assertion);
		if(expectedExpression==null){
			DefaultSingleAssertPredicate defaultSinglePredicate=oneArgumentAssertExpectedOutcome(assertion);	
			predicate.predicateString=defaultSinglePredicate.predicateString;
			predicate.assertion=defaultSinglePredicate.assertion;
			predicate.expectedValueObject=defaultSinglePredicate.expectedValueObject;
		}
		else{
				
			predicate.assertion=assertion.getName();						
			
			ASTNode constant=findConstant(expectedExpression);
			if(constant!=null){
				predicate.expectedValueObject=constant;
			}
			else{
				predicate.expectedValueObject=expectedExpression;
			}
		}
		
	 super.predicate=predicate;	
	}

}
