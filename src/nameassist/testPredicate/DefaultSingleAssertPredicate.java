package nameassist.testPredicate;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;

import com.google.common.base.Joiner;

import edu.udel.headless.JavadocReturnFinder;
import edu.udel.headless.action.ActionGraph;
import edu.udel.headless.action.ActionGraphVisitor;
import nameassist.nameTemplates.DefaultSingleAssertTemplate;
import nameassist.nameTemplates.TestNameTemplate;
import nameassist.utils.Utils;

public class DefaultSingleAssertPredicate extends TestPredicate {
	
	public String actualValueMethodName;
	public IMethod actualValueMethod;
	public ASTNode actualValueObject;
	public Set<ASTNode> actualValueModifiers;
	
	public ASTNode assertion;
	public String predicateString;
	
	public ASTNode expectedValueObject;
	public Set<ASTNode> expectedValueModifiers;
	
	public String actualValueObjectType;
	public String expectedValueObjectType;
	
	
	public DefaultSingleAssertPredicate(){
		this.actualValueMethodName=null;
		this.actualValueObject=null;
		this.assertion=null;
		this.predicateString=null;
		this.expectedValueObject=null;
		this.actualValueModifiers=new HashSet<ASTNode>();
		this.expectedValueModifiers=new HashSet<ASTNode>();
	}
	
	public void findTypeofObject(){
		 actualValueObjectType=actualValueObject!=null?variableTable.get(actualValueObject.toString()):null;
		 expectedValueObjectType=expectedValueObject!=null?variableTable.get(expectedValueObject.toString()):null;
		 
//		 if(Utils.isConstant(expectedValueObject)&&expectedValueObjectType==null&&actualValueMethodName!=null && actualValueMethodName.startsWith("get")){			
//			 expectedValueObjectType=actualValueMethodName.replace("get", ""); 
//		 }
		 
		 if(Utils.isConstant(expectedValueObject)&&expectedValueObjectType==null&&actualValueMethodName!=null){			
			 Optional<String> returnType=JavadocReturnFinder.find(actualValueMethod);
			 if(returnType.isPresent()){
				 expectedValueObjectType=returnType.get();
			 }
			 else if(actualValueMethodName.startsWith("get")){
				 expectedValueObjectType=actualValueMethodName.replace("get", ""); 
			 }
		 }
		 
	}
	
	
	public String toString(){						
		findTypeofObject();
		return String.format(
				  "actual value method name: %s\n"
				+ "actual value object: %s%s \n"
				+ "actual value modifiers: [%s]\n"
				+ "assertion method: %s\n"
				+ "expected value method: %s\n"
				+ "expected value object: %s%s\n"
				+ "expected value modifers: %s\n", 
				actualValueMethodName,
				actualValueObject,
				actualValueObjectType==null?"":"::"+actualValueObjectType,
				actualValueModifiers.stream().map(ASTNode::toString).collect(Collectors.joining(",")),
				assertion,
				predicateString,
				expectedValueObject,
				expectedValueObjectType==null?"":"::"+expectedValueObjectType,
				expectedValueModifiers.stream().map(ASTNode::toString).collect(Collectors.joining(","))												
				);
	}


	@Override
	public String translate() {
		findTypeofObject();
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public boolean hasSamePredicateFacts(DefaultSingleAssertPredicate facts){
		if(facts==null) return false;
		
		boolean a=(assertion==null && facts.assertion == null) ||
				(assertion!=null && facts.assertion != null &&
						assertion.toString().equals(facts.assertion.toString()));
		
		boolean p=(predicateString==null && facts.predicateString==null) ||
				(predicateString!=null && facts.predicateString!=null && predicateString.equals(facts.predicateString));
	   
		boolean po=(expectedValueObject==null && facts.expectedValueObject == null) ||
				(expectedValueObject!=null && facts.expectedValueObject != null &&
				expectedValueObject.toString().equals(facts.expectedValueObject.toString()));
		
		return a && p && po;
	}

	@Override
	public TestNameTemplate getTemplate(ActionGraph actionGraph) {
			String actual=actualValueMethodName;
			String assertion=predicateString!=null?predicateString:this.assertion.toString();
			String expected=null!=expectedValueObjectType?expectedValueObjectType:(null!=expectedValueObject?expectedValueObject.toString():null);
			
			List<ASTNode> scenarioList=ActionGraphVisitor.visit(actionGraph, 1);
			
			if(scenarioList.size()>0 && (scenarioList.get(0) instanceof MethodInvocation)){
				MethodInvocation method=(MethodInvocation)scenarioList.get(0);
				IMethod iMethod=(IMethod)((IMethodBinding)method.resolveMethodBinding()).getJavaElement();
				if(null!=iMethod && iMethod.equals(actualValueMethod)){
					scenarioList.remove(0);
				}
			}
			
			String featureUnderTest=null;
			if(scenarioList.size()>0){
				featureUnderTest=scenarioList.get(0).toString();
				scenarioList.remove(0);
			}
			
									
			return new DefaultSingleAssertTemplate(featureUnderTest, actual, assertion, expected, scenarioList);

	}	
}
