package nameassist.testPredicate;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTNode;

import edu.udel.headless.action.ActionGraph;
import nameassist.nameTemplates.TestNameTemplate;
import nameassist.utils.Utils;

public class DefaultMultipleAssertContiguousPredicate extends TestPredicate{
	
	public List<String> actualValueMethodNames;
	public ASTNode actualValueObject;
	public Set<ASTNode> actualValueModifiers;
	
	public ASTNode assertion;
	public String predicateString;
	
	public ASTNode expectedValueObject;
	public Set<ASTNode> expectedValueModifiers;
	
	public String actualValueObjectType;
	public String expectedValueObjectType;
	
	public DefaultMultipleAssertContiguousPredicate() {
		this.actualValueMethodNames=new LinkedList<String>();
		this.actualValueObject=null;
		this.assertion=null;
		this.predicateString=null;
		this.actualValueModifiers=new HashSet<ASTNode>();
		this.expectedValueModifiers=new HashSet<ASTNode>();		
	}
	
	
	public void findTypeofObject(){
		 actualValueObjectType=actualValueObject!=null?variableTable.get(actualValueObject.toString()):null;
		 expectedValueObjectType=expectedValueObject!=null?variableTable.get(expectedValueObject.toString()):null;				 
	}
	
	

	@Override
	public String toString() {
		findTypeofObject();
		return String.format(
				  "actual value method name: %s\n"
				+ "actual value object: %s%s \n"
				+ "actual value modifiers: [%s]\n"
				+ "assertion method: %s\n"
				+ "expected value method: %s\n"
				+ "expected value object: %s%s\n"
				+ "expected value modifers: %s\n", 
				actualValueMethodNames,
				actualValueObject,
				actualValueObjectType==null?"":"::"+actualValueObjectType,
				actualValueModifiers.stream().map(ASTNode::toString).collect(Collectors.joining(",")),
				assertion,
				predicateString,
				expectedValueObject,
				expectedValueObjectType==null?"":"::"+expectedValueObjectType,
				expectedValueModifiers.stream().map(ASTNode::toString).collect(Collectors.joining(","))												
				);
	}

	@Override
	public String translate() {
		findTypeofObject();
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public TestNameTemplate getTemplate(ActionGraph actionGraph) {
		// TODO Auto-generated method stub
		return null;
	}



}
