package nameassist.testPredicate;

import java.util.HashMap;

import org.eclipse.jdt.internal.core.SetVariablesOperation;

import edu.udel.headless.action.ActionGraph;
import nameassist.nameTemplates.TestNameTemplate;

public abstract class TestPredicate {
	
	protected HashMap<String,String> variableTable;
	
	public void setVariableTable(HashMap<String,String> map){
		this.variableTable=map;
	}
	
	public abstract String toString();
	
	public abstract String  translate();
	
	public abstract TestNameTemplate getTemplate(ActionGraph actionGraph);

}
