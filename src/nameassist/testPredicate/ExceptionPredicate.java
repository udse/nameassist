package nameassist.testPredicate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTNode;

import edu.udel.headless.action.ActionGraph;
import nameassist.nameTemplates.TestNameTemplate;

public class ExceptionPredicate extends TestPredicate{
	
	public String actionMethodName;
	public ASTNode actionObject;
	public Set<ASTNode> actionModifiers;
	public boolean throwException;
	public ASTNode exceptionType;
	public List<ASTNode> assertionsInCatchBlock;
	public String actionObjectType;

	
	
	
	public  ExceptionPredicate() {
		this.actionMethodName=null;
		this.actionObject=null;
		this.actionModifiers=new HashSet<ASTNode>();
		this.throwException=false;
		this.exceptionType=null;
		this.assertionsInCatchBlock=null;
		
	}
	
	public void findTypeofObject(){
		 actionObjectType=actionObject!=null?variableTable.get(actionObject.toString()):null;
	}
	
	

	@Override
	public String toString() {
		findTypeofObject();
		return String.format(
				  "action: %s\n"
				+ "actionObject: %s%s \n"
				+ "action modifiers: [%s]\n"
				+ "throw exception? : %s\n"
				+ "exception type: %s\n"
				+ "assertions in catch block: %s\n", 
				actionMethodName,
				actionObject,
				actionObjectType==null?"":"::"+actionObjectType,
				actionModifiers.stream().map(ASTNode::toString).collect(Collectors.joining(",")),
				throwException,
				exceptionType,			
				assertionsInCatchBlock.stream().map(ASTNode::toString).collect(Collectors.joining(","))												
				);
	}

	@Override
	public String translate() {
		findTypeofObject();
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TestNameTemplate getTemplate(ActionGraph actionGraph) {
		// TODO Auto-generated method stub
		return null;
	}

}
