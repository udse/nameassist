package nameassist.testPredicate;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTNode;

import edu.udel.headless.action.ActionGraph;
import nameassist.nameTemplates.TestNameTemplate;

public class BeforeAndAfterPredicate extends TestPredicate {
	
	public String middleActionMethodName;
	public ASTNode middleActionObject;
	public Set<ASTNode> middleActionModifiers;
	public boolean change;
	public DefaultSingleAssertPredicate assertionFacts;
	public String middleActionObjectType;
	
	
	
	public BeforeAndAfterPredicate() {
		// TODO Auto-generated constructor stub
		this.middleActionMethodName=null;
		this.middleActionObject=null;
		
		this.change=true;
		this.middleActionModifiers=new HashSet<ASTNode>();
		this.assertionFacts=new DefaultSingleAssertPredicate();
	}
	
	public void findTypeofObject(){
		 middleActionObjectType=middleActionObject!=null?variableTable.get(middleActionObject.toString()):null;
	}
	

	@Override
	public String toString() {
		assertionFacts.setVariableTable(variableTable);
		findTypeofObject();
		return String.format(
				  "middle action: %s\n"
				+ "middle actionObject: %s%s \n"
				+ "middle action modifiers: [%s]\n"
				+ "change? :%s\n\n"
				+ "%s\n",
				middleActionMethodName,
				middleActionObject,
				middleActionObjectType==null?"":"::"+middleActionObjectType,
				middleActionModifiers.stream().map(ASTNode::toString).collect(Collectors.joining(",")),
				change,
				assertionFacts
				);
	}

	@Override
	public String translate() {
		assertionFacts.setVariableTable(variableTable);
		findTypeofObject();
		assertionFacts.findTypeofObject();
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TestNameTemplate getTemplate(ActionGraph actionGraph) {
		// TODO Auto-generated method stub
		return null;
	}



}
