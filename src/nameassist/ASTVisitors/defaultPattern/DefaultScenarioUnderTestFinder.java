package nameassist.ASTVisitors.defaultPattern;

import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import com.google.common.collect.ImmutableSet;

public class DefaultScenarioUnderTestFinder extends ASTVisitor {

	private final ImmutableSet<String> identifiers;
	private final ImmutableSet.Builder<ASTNode> builder;

	public DefaultScenarioUnderTestFinder(final Iterable<String> identifiers) {
		this.identifiers = ImmutableSet.copyOf(identifiers);
		this.builder = ImmutableSet.builder();
	}
	
	public boolean visit(VariableDeclarationFragment fragment) {
		fragment.accept(new ASTVisitor() {
			public boolean visit(SimpleName name) {
				if (DefaultScenarioUnderTestFinder.this.identifiers.contains(name.getIdentifier())) {
					if(!(fragment.getInitializer() instanceof ClassInstanceCreation))					
					DefaultScenarioUnderTestFinder.this.builder.add(fragment);
					
				}
				return true;
			}
		});

		return false;		
	}

	public boolean visit(FieldAccess expression) {
		expression.accept(new ASTVisitor() {
			public boolean visit(SimpleName name) {
				if (DefaultScenarioUnderTestFinder.this.identifiers.contains(name.getIdentifier())) {
					DefaultScenarioUnderTestFinder.this.builder.add(expression);
				}
				return true;
			}
		});

		return false;		
	}
	
	public boolean visit(MethodInvocation expression) {
		expression.accept(new ASTVisitor() {
			public boolean visit(SimpleName name) {
				if (DefaultScenarioUnderTestFinder.this.identifiers.contains(name.getIdentifier())) {
					DefaultScenarioUnderTestFinder.this.builder.add(expression);
				}
				return true;
			}
		});

		return false;		
	}

	public boolean visit(SuperFieldAccess expression) {
		expression.accept(new ASTVisitor() {
			public boolean visit(SimpleName name) {
				if (DefaultScenarioUnderTestFinder.this.identifiers.contains(name.getIdentifier())) {
					DefaultScenarioUnderTestFinder.this.builder.add(expression);
				}
				return true;
			}
		});

		return false;		
	}

	public boolean visit(SuperMethodInvocation expression) {
		expression.accept(new ASTVisitor() {
			public boolean visit(SimpleName name) {
				if (DefaultScenarioUnderTestFinder.this.identifiers.contains(name.getIdentifier())) {
					DefaultScenarioUnderTestFinder.this.builder.add(expression);
				}
				return true;
			}
		});

		return false;		
	}
	
	public boolean visit(VariableDeclarationExpression expression) {
		expression.accept(new ASTVisitor() {
			public boolean visit(SimpleName name) {
				if (DefaultScenarioUnderTestFinder.this.identifiers.contains(name.getIdentifier())) {
					DefaultScenarioUnderTestFinder.this.builder.add(expression);
				}
				return true;
			}
		});

		return false;
	}			
	
	public Set<ASTNode> uses() {
		return builder.build();
	}
}