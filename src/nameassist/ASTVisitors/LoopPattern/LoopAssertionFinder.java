package nameassist.ASTVisitors.LoopPattern;

import java.util.List;
import java.util.Set;

import nameassist.utils.Utils;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.WhileStatement;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class LoopAssertionFinder extends ASTVisitor {

	private final ImmutableList.Builder<MethodInvocation> builder;
	private Statement loopStatement;

	public LoopAssertionFinder() {
		this.builder = ImmutableList.builder();
	}
		
	
	public boolean visit(DoStatement statement) {
		statement.accept(new ASTVisitor(){
			public boolean visit(MethodInvocation invocation) {
				if (Utils.isAssertion(invocation)) {								
					LoopAssertionFinder.this.builder.add(invocation);
					LoopAssertionFinder.this.loopStatement=statement;
					
				}
				return false;
			}
		});
		return false;		
	}
	
	public boolean visit(ForStatement statement) {
		statement.accept(new ASTVisitor(){
			public boolean visit(MethodInvocation invocation) {
				if (Utils.isAssertion(invocation)) {								
					LoopAssertionFinder.this.builder.add(invocation);
					LoopAssertionFinder.this.loopStatement=statement;
					
				}
				return false;
			}
		});	
		return false;
	}
	
	
	public boolean visit(WhileStatement statement) {
		statement.accept(new ASTVisitor(){
			public boolean visit(MethodInvocation invocation) {
				if (Utils.isAssertion(invocation)) {								
					LoopAssertionFinder.this.builder.add(invocation);
					LoopAssertionFinder.this.loopStatement=statement;
					
				}
				return false;
			}
		});
		return false;
	}	
	
	public Statement getLoopStatement(){
		return this.loopStatement;
	}
	
			
	public List<MethodInvocation> assertionsInLoop() {
		return builder.build();
	}
}