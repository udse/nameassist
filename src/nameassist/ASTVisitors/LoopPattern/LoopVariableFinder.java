package nameassist.ASTVisitors.LoopPattern;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression.Operator;
import org.eclipse.jdt.core.dom.SimpleName;

import nameassist.utils.Utils;

public class LoopVariableFinder extends ASTVisitor {
	
	private SimpleName loopVariable;
	
	public boolean visit(PostfixExpression expression) {
		Expression expr=expression.getOperand();
		if(expr instanceof SimpleName){
			loopVariable=(SimpleName)expr;
		}
		return false;		
	}
	
	public boolean visit(PrefixExpression expression) {
		Operator operator=expression.getOperator();
		Expression expr=expression.getOperand();
		if(expr instanceof SimpleName && (operator.equals(Operator.INCREMENT)||operator.equals(Operator.DECREMENT))){
			loopVariable=(SimpleName)expr;
		}
		return false;		
	}
	
	public SimpleName getLoopVariable(){
		return loopVariable;
	}

}
