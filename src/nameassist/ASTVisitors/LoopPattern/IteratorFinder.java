package nameassist.ASTVisitors.LoopPattern;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

public class IteratorFinder extends ASTVisitor {
	
	private Expression iterator;
	private Expression iterateItem;
	
	public boolean visit(MethodInvocation invocation) {
		
		if(invocation.getName().toString().equals("next")){			
			iterator=invocation.getExpression();
			ASTNode parentNode=iterator.getParent();
			while(!(parentNode instanceof Block)){
				if(parentNode instanceof Assignment){
					iterateItem=((Assignment)parentNode).getLeftHandSide();
					return false;
				}
				if(parentNode instanceof VariableDeclarationFragment){
					iterateItem=((VariableDeclarationFragment)parentNode).getName();					
					return false;
				}
				parentNode=parentNode.getParent();
			}
			
			return false;
		}
		return true;			
	}
	
	public Expression getIterator(){
		return iterator;
	}
	
	public Expression getIterateItem(){
		return iterateItem;
	}

}
