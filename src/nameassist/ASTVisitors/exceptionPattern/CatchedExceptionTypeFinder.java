package nameassist.ASTVisitors.exceptionPattern;

import java.util.List;
import java.util.Set;

import nameassist.utils.Utils;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class CatchedExceptionTypeFinder extends ASTVisitor {

	private ASTNode exceptionType;


	
	public boolean visit(CatchClause catchClause) {
		exceptionType=catchClause.getException().getType();				
		return false;		
	}

			
	public ASTNode exceptionType() {
		return exceptionType;
	}
}