package nameassist.ASTVisitors.exceptionPattern;

import java.util.List;
import java.util.Set;

import nameassist.utils.Utils;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class ExceptionScenarioUnderTestFinder extends ASTVisitor {

	private final ImmutableList.Builder<ASTNode> builder;

	public ExceptionScenarioUnderTestFinder() {
		this.builder = ImmutableList.builder();
	}
	
	public boolean visit(TryStatement tryStatement) {
		tryStatement.getBody().accept(new ASTVisitor(){			
			public boolean visit(MethodInvocation methodInvocation) {
				if (!Utils.isFailAssertion(methodInvocation))
					ExceptionScenarioUnderTestFinder.this.builder.add(methodInvocation);					
				return false;
			}
			
			public boolean visit(ClassInstanceCreation expression) {
					ExceptionScenarioUnderTestFinder.this.builder.add(expression);					
				return false;
			}
			
			
		});						
		return false;		
	}

			
	public List<ASTNode> nonAssertionInTryBlock() {
		return builder.build();
	}
}