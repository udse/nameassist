package nameassist.ASTVisitors.exceptionPattern;

import java.util.List;
import java.util.Set;

import nameassist.utils.Utils;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class AssertionsInCatchBlockFinder extends ASTVisitor {

	private final ImmutableList.Builder<ASTNode> builder;

	public AssertionsInCatchBlockFinder() {
		this.builder = ImmutableList.builder();
	}
	
	public boolean visit(CatchClause catchClause) {
		catchClause.accept(new ASTVisitor(){			
			public boolean visit(MethodInvocation invocation) {				
				if (Utils.isAssertion(invocation)||Utils.isFailAssertion(invocation)) {
					
					AssertionsInCatchBlockFinder.this.builder.add(invocation);
					
				}
				return true;
			}			
		});						
		return false;		
	}

			
	public List<ASTNode> assertionsInCatchBlock() {
		return builder.build();
	}
}