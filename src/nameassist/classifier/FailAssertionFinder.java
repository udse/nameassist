package nameassist.classifier;

import nameassist.utils.Utils;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;


public class FailAssertionFinder extends ASTVisitor {
	
	private boolean hasFailAssertion;

	public FailAssertionFinder() {
		hasFailAssertion=false;
	}

	public boolean visit(MethodInvocation methodInvocation) {
		
		if(Utils.isFailAssertion(methodInvocation)){
			hasFailAssertion=true;
			return false;
		}
		else{
			return true;	
		}
	}
	
	public boolean hasFailAssertion(){
		return hasFailAssertion;
	}


}
