package nameassist.classifier;

import nameassist.utils.Utils;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;


public class AssertionVisitor extends ASTVisitor {
	
	private boolean hasAssertion;

	public AssertionVisitor() {
		hasAssertion=false;
	}

	public boolean visit(MethodInvocation methodInvocation) {
		
		if(Utils.isAssertion(methodInvocation)){
			hasAssertion=true;
			return false;
		}
		else{
			return true;	
		}
	}
	
	public boolean hasAssertion(){
		return hasAssertion;
	}


}
