package nameassist.classifier;

import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.WhileStatement;

import com.google.common.collect.ImmutableSet;

public class LoopPatternFinder extends ASTVisitor {

	private boolean hasAssertioninLoop;	

	public LoopPatternFinder() {
		hasAssertioninLoop=false;
	}
	
	public boolean findAssertion(Statement statement){
		AssertionVisitor assertionFinder=new AssertionVisitor();
		statement.accept(assertionFinder);
		if(assertionFinder.hasAssertion()) return true;
		else return false;
	}

	public boolean visit(DoStatement statement) {		
		if(findAssertion(statement)){
			hasAssertioninLoop=true;
			return false;
		}
		else return true;
		
	}
	
	public boolean visit(ForStatement statement) {		
		if(findAssertion(statement)){
			hasAssertioninLoop=true;
			return false;
		}
		else return true;
	}
	
	
	public boolean visit(WhileStatement statement) {		
		if(findAssertion(statement)){
			hasAssertioninLoop=true;
			return false;
		}
		else return true;
	}	

	public boolean hasAssertionInLoop(){
		return hasAssertioninLoop;
	}
	
	
}