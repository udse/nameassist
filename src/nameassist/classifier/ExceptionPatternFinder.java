package nameassist.classifier;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.TryStatement;

public class ExceptionPatternFinder extends ASTVisitor {

	private boolean hasAssertioninTryCatchBlock;	

	public ExceptionPatternFinder() {
		hasAssertioninTryCatchBlock=false;
	}
	
	public boolean findAssertion(ASTNode node){
		AssertionVisitor assertionVisitor =new AssertionVisitor();
		node.accept(assertionVisitor);
		if(assertionVisitor.hasAssertion()) {
			return true;
		}
		else return false;
	}
	
	public boolean findFail(ASTNode node){
		FailAssertionFinder failAssertionFinder=new FailAssertionFinder();
		node.accept(failAssertionFinder);
		if(failAssertionFinder.hasFailAssertion()) return true;
		else return false;
	}
	
	public boolean visit(TryStatement tryStatement) {		
		if(findFail(tryStatement.getBody())){
			hasAssertioninTryCatchBlock=true;
			return false;
		}
		else return true;
		
	}

	public boolean visit(CatchClause catchClause) {		
		if(findAssertion(catchClause)||findFail(catchClause)){
			hasAssertioninTryCatchBlock=true;
			return false;
		}
		else return true;
		
	}
	

	public boolean hasAssertionInTryCatchBlock(){
		return hasAssertioninTryCatchBlock;
	}
	
}