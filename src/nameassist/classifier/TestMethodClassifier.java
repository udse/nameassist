package nameassist.classifier;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import nameassist.utils.AssertionFinder;
import nameassist.utils.Utils;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;

import com.google.common.base.Predicate;

public class TestMethodClassifier {
	
	public static TestMethodType classify(MethodDeclaration methodDeclaration) {
		
		//identify loop pattern
		LoopPatternFinder loopFinder =new LoopPatternFinder();
		methodDeclaration.accept(loopFinder);
		if(loopFinder.hasAssertionInLoop()) return TestMethodType.LOOP;
		
		//identify exception pattern
		ExceptionPatternFinder exceptionFinder = new ExceptionPatternFinder();
		methodDeclaration.accept(exceptionFinder);
		if(exceptionFinder.hasAssertionInTryCatchBlock()) return TestMethodType.EXCEPTION;
		
		//identify before and after pattern
		List<MethodInvocation> seq=getMethodInvocations(methodDeclaration);
					
		for(int i=1;i<seq.size()-1;i++){
			
			if(Utils.isAssertion(seq.get(i-1))&&(!Utils.isAssertion(seq.get(i)))&&Utils.isAssertion(seq.get(i+1))){			
				return TestMethodType.BEFORE_AND_AFTER;				
			}
		}
		
		//identify default_single_assert pattern		
		if(AssertionFinder.assertIn(methodDeclaration).size()==1) return TestMethodType.DEFAULT_SINGLE_ASSERT;	
		
		//identify default_multiple_assert_contiguous pattern and default_multiple_assert_separate pattern
		if(isContiguous(methodDeclaration)) return TestMethodType.DEFAULT_MULTIPLE_ASSERT_CONTIGUOUS;
		else return TestMethodType.DEFAULT_MULTIPLE_ASSERT_SEPARATE;						
		
	}
	
	
	public static List<MethodInvocation> getMethodInvocations(MethodDeclaration methodDeclaration){
		List statements=methodDeclaration.getBody().statements();
		List<MethodInvocation> seq=new ArrayList<MethodInvocation>();
		for(Object obj:statements){
			if(obj instanceof ExpressionStatement){
				ExpressionStatement exprStatement=(ExpressionStatement)obj;
				Expression expression=exprStatement.getExpression();
				if(expression instanceof MethodInvocation)seq.add((MethodInvocation)expression);					
			}
		}
		return seq;
	}
	
			
	private static boolean isContiguous(MethodDeclaration methodDeclaration){
		LinkedList<Statement> statements=new LinkedList<Statement>();
		statements.addAll(methodDeclaration.getBody().statements());
		
		//remove preceding non-assertion statement 
		while(!statements.isEmpty()){
			Statement statement=statements.getFirst();
			if(!(statement instanceof ExpressionStatement)) {
				statements.removeFirst();
				continue;				
			}
			
			Expression expr=((ExpressionStatement)statement).getExpression();
			if(!(expr instanceof MethodInvocation)){
				statements.removeFirst();
				continue;
			}
			if(!Utils.isAssertion((MethodInvocation)expr)){
				statements.removeFirst();
			}
			else{
				break;
			}
			
		}
		
		//remove succeeding non-assertion statement 
		while(!statements.isEmpty()){
			Statement statement=statements.getLast();
			if(!(statement instanceof ExpressionStatement)) {
				statements.removeLast();
				continue;				
			}
			
			Expression expr=((ExpressionStatement)statement).getExpression();
			if(!(expr instanceof MethodInvocation)){
				statements.removeLast();
				continue;
			}
			if(!Utils.isAssertion((MethodInvocation)expr)){
				statements.removeLast();
			}
			else{
				break;
			}
			
		}
		
		for(Statement statement:statements){
			if(!(statement instanceof ExpressionStatement)) {
				return false;
			}
			Expression expr=((ExpressionStatement)statement).getExpression();
			if(!(expr instanceof MethodInvocation)){
				return false;
			}
			if(!Utils.isAssertion((MethodInvocation)expr)){
				return false;
			}
		}	
		return true;
	}
		

}
