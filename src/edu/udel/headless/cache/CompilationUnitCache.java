package edu.udel.headless.cache;

import java.util.Optional;

import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class CompilationUnitCache {

	private static final LoadingCache<ITypeRoot, Optional<CompilationUnit>> cache = CacheBuilder.newBuilder()
			.build(new CacheLoader<ITypeRoot, Optional<CompilationUnit>>() {
				public Optional<CompilationUnit> load(ITypeRoot root) {
					try {
						ASTParser parser = ASTParser.newParser(AST.JLS8);
						parser.setKind(ASTParser.K_COMPILATION_UNIT);
						parser.setResolveBindings(true);
						parser.setBindingsRecovery(true);
						parser.setStatementsRecovery(true);
						parser.setSource(root);
						return Optional.of((CompilationUnit) parser.createAST(null));
					} catch (IllegalStateException e) {
						return Optional.empty();
					}
				}
			});

	public static Optional<CompilationUnit> get(final ITypeRoot root) {
		return cache.getUnchecked(root);
	}
}
