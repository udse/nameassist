package edu.udel.headless.cache;

import java.util.Optional;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import edu.udel.headless.util.ASTNodeUtils;

public class MethodDeclarationCache {

	private static final LoadingCache<IMethod, Optional<MethodDeclaration>> cache = CacheBuilder.newBuilder()
			.build(new CacheLoader<IMethod, Optional<MethodDeclaration>>() {
				public Optional<MethodDeclaration> load(IMethod method) {

					IType testClass = method.getDeclaringType();

					return CompilationUnitCache.get(testClass.getCompilationUnit())
							.flatMap(cu -> 
									ASTNodeUtils.find(cu, MethodDeclaration.class)
										.filter(decl -> decl.getName().getIdentifier().equals(method.getElementName()))
										.findFirst()
					);
				}
			});

	public static Optional<MethodDeclaration> get(final IMethod method) {
		return cache.getUnchecked(method);
	}
}
