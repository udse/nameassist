package edu.udel.headless.cache;

import java.util.Optional;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class JavadocCache {

	private static final LoadingCache<IMethod, Optional<Javadoc>> cache = CacheBuilder.newBuilder()
			.build(new CacheLoader<IMethod, Optional<Javadoc>>() {
				public Optional<Javadoc> load(IMethod method) {
					return CompilationUnitCache.get(method.getTypeRoot())
							.map(cu -> (MethodDeclaration) cu.findDeclaringNode(method.getKey()))
							.map(MethodDeclaration::getJavadoc);
				}
			});
	
	public static Optional<Javadoc> get(final IMethod method) {
		return cache.getUnchecked(method);
	}
}
