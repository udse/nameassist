package edu.udel.headless;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Type;

import edu.udel.headless.cache.MethodDeclarationCache;
import edu.udel.headless.util.ASTNodeUtils;

public class ClassUnderTestFinder {

	public static Optional<IType> checkForClassWithMatchingName(final IMethod testMethod) {
		final IType testClass = testMethod.getDeclaringType();
		final IJavaProject project = testClass.getJavaProject();

		String classUnderTestName = testClass.getFullyQualifiedName().replace("Test", "");
		try {
			IType underTestClass = project.findType(classUnderTestName);
			if(null!=underTestClass) return Optional.of(underTestClass);
			else return Optional.empty();
		} catch (JavaModelException e) {
			return Optional.empty();
		}
	}

	// check for void method before assert
	public static Optional<IType> checkForVoidMethodBeforeAssert(final IMethod testMethod) {
		final IType testClass = testMethod.getDeclaringType();
		final IJavaProject project = testClass.getJavaProject();

		MethodDeclaration method = MethodDeclarationCache.get(testMethod).get();

		MethodInvocation assertion = ASTNodeUtils.find(method, MethodInvocation.class)
				.filter(ASTNodeUtils::isAssertion)
				.sorted(Comparator.comparingInt(ASTNode::getStartPosition))
				.findFirst()
				.orElseThrow(RuntimeException::new);

		Optional<String> classUnderTestName = ASTNodeUtils.find(method, MethodInvocation.class)
				.filter(invocation -> {
					IMethodBinding binding = invocation.resolveMethodBinding();
					ITypeBinding returnType = binding.getReturnType();
					return returnType.isPrimitive() && "void".equals(returnType.getName());
				})
				.filter(invocation -> !ASTNodeUtils.isAssertion(invocation))
				.filter(invocation -> invocation.getStartPosition() < assertion.getStartPosition())
				.sorted(Comparator.comparingInt(ASTNode::getStartPosition).reversed())
				.findFirst()
				.flatMap(action -> Optional.ofNullable(action.getExpression()))
				.map(Expression::resolveTypeBinding)
				.map(ITypeBinding::getQualifiedName);

		if (classUnderTestName.isPresent()) {
			try {
				IType underTestClass = project.findType(classUnderTestName.get());
				return Optional.ofNullable(underTestClass);
			} catch (JavaModelException e) {
			}
		}

		return Optional.empty();
	}

	public static Optional<IType> checkForNewInTest(final IMethod testMethod) {
		final IType testClass = testMethod.getDeclaringType();
		final IJavaProject project = testClass.getJavaProject();

		MethodDeclaration method = MethodDeclarationCache.get(testMethod).get();

		Optional<String> classUnderTestName = ASTNodeUtils.find(method, ClassInstanceCreation.class)
				.sorted(Comparator.comparingInt(ASTNode::getStartPosition))
				.findFirst()
				.map(ClassInstanceCreation::getType)
				.map(Type::resolveBinding)
				.map(ITypeBinding::getQualifiedName);

		if (classUnderTestName.isPresent()) {
			IType underTestClass = null;
			try {
				underTestClass = project.findType(classUnderTestName.get());
			} catch (JavaModelException e) {
			}
			return Optional.ofNullable(underTestClass);
		}

		return Optional.empty();
	}

	public static Optional<IType> checkForNewInSetup(final IMethod testMethod) {
		final IType testClass = testMethod.getDeclaringType();
		final IJavaProject project = testClass.getJavaProject();

		Optional<MethodDeclaration> setup = MethodDeclarationCache
				.get(testMethod.getDeclaringType().getMethod("setUp", new String[] {}));
		
		if(!setup.isPresent()) return Optional.empty();

		Optional<String> classUnderTestName = ASTNodeUtils.find(setup.get(), ClassInstanceCreation.class)
				.sorted(Comparator.comparingInt(ASTNode::getStartPosition))
				.findFirst()
				.map(ClassInstanceCreation::getType)
				.map(Type::resolveBinding)
				.map(ITypeBinding::getQualifiedName);

		if (classUnderTestName.isPresent()) {
			try {
				IType underTestClass = project.findType(classUnderTestName.get());
				return Optional.ofNullable(underTestClass);
			} catch (JavaModelException e) {
			}
		}

		return Optional.empty();
	}

	public static Optional<IType> find(final IMethod testMethod) throws JavaModelException {
		return Stream.of(
				checkForClassWithMatchingName(testMethod),
				checkForVoidMethodBeforeAssert(testMethod),
				checkForNewInTest(testMethod),
				checkForNewInSetup(testMethod))
				.filter(Optional::isPresent)
				.findFirst()
				.orElse(Optional.empty());
	}
}
