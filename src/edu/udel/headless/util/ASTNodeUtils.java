package edu.udel.headless.util;

import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.StringLiteral;

import com.google.common.collect.ImmutableSet;

public class ASTNodeUtils {

	private static final class TypeFinder<T extends ASTNode> extends ASTVisitor {

		private final Class<T> type;
		private final Stream.Builder<T> builder;

		public TypeFinder(final Class<T> type) {
			this.type = type;
			this.builder = Stream.builder();
		}

		@Override
		public boolean visit(Javadoc node) {
			return true;
		}

		@Override
		public void postVisit(ASTNode node) {
			if (type.isInstance(node)) {
				builder.add(type.cast(node));
			}
		}

		public Stream<T> nodes() {
			return builder.build();
		}
	}

	public static <T extends ASTNode> Stream<T> find(final ASTNode root, final Class<T> cls) {
		TypeFinder<T> finder = new TypeFinder<>(cls);
		root.accept(finder);
		return finder.nodes();
	}

	private static final class InstanceFinder extends ASTVisitor {

		private final ASTNode instance;
		private boolean found;

		public InstanceFinder(final ASTNode instance) {
			this.instance = instance;
			found = false;
		}

		@Override
		public void postVisit(ASTNode node) {
			if (instance.equals(node)) {
				found = true;
			}
		}

		public boolean found() {
			return found;
		}
	}

	public static boolean contains(final ASTNode root, final ASTNode node) {
		InstanceFinder finder = new InstanceFinder(node);
		root.accept(finder);
		return finder.found();
	}

	private static final Set<String> assertMethodNames = ImmutableSet.of(
			"assertTrue",
			"assertFalse",
			"assertEquals",
			"assertSame",
			"assertNull",
			"assertNotNull",
			"assertListEquals");

	public static boolean isAssertion(MethodInvocation invocation) {
		return assertMethodNames.contains(invocation.getName().toString());
	}

	public static boolean isFailAssertion(MethodInvocation invocation) {
		return invocation.getName().toString().equals("fail");
	}

	public static boolean isConstant(ASTNode node) {
		if (node instanceof BooleanLiteral ||
				node instanceof NullLiteral ||
				node instanceof CharacterLiteral ||
				node instanceof NumberLiteral ||
				node instanceof StringLiteral) {
			return true;
		}
		return false;
	}

	public static boolean isGetter(ASTNode node) {

		if (!(node instanceof MethodInvocation)) {
			return false;
		}

		MethodInvocation invocation = (MethodInvocation) node;

		String name = invocation.getName().toString();

		if (name.startsWith("get")) {
			return true;
		}

		Expression obj = invocation.getExpression();
		
		if(null==obj) {
			return false;
		}
		
		ITypeBinding typeBinding = obj.resolveTypeBinding();

		IVariableBinding[] fieldBindings = typeBinding.getDeclaredFields();
		for (IVariableBinding fieldBinding : fieldBindings) {
			if (name.equals(fieldBinding.getName())) {
				return true;
			}
		}

		return false;
	}

}