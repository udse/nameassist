package edu.udel.headless.util;

import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;

public class TagElementUtils {

	public static String toString(final TagElement tag) {
		return ASTNodeUtils.find(tag, TextElement.class)
				.map(TextElement::getText)
				.collect(Collectors.joining()).trim();
	}
	
}
