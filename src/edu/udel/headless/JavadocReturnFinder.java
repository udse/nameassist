package edu.udel.headless;

import java.util.Optional;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.TagElement;

import edu.udel.headless.cache.JavadocCache;
import edu.udel.headless.util.ASTNodeUtils;
import edu.udel.headless.util.TagElementUtils;

public class JavadocReturnFinder {
	
	public static Optional<String> find(final IMethod method) {
		return JavadocCache.get(method).flatMap(javadoc -> {
			return ASTNodeUtils.find(javadoc, TagElement.class)
					.filter(tag -> TagElement.TAG_RETURN.equals(tag.getTagName()))
					.findFirst()
					.map(TagElementUtils::toString);
		});
	}
}
