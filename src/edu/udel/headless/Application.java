package edu.udel.headless;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.udel.graph.DOTExporter;
import edu.udel.headless.action.ActionGraph;
import edu.udel.headless.action.ActionGraphBuilder;
import edu.udel.headless.action.ActionGraphVisitor;
import edu.udel.headless.cache.MethodDeclarationCache;
import edu.udel.headless.util.ASTNodeUtils;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import nameassist.finderFactories.FactsFinderFactory;
import nameassist.nameTemplates.TestNameTemplate;
import nameassist.testPredicateFinder.FactsFinder;

public class Application implements IApplication {

	@Override
	public Object start(IApplicationContext applicationContext) throws Exception {

		OptionParser parser = new OptionParser();

		OptionSpec<File> configSpec = parser.accepts("config").withRequiredArg().ofType(File.class);
		OptionSpec<File> outputDirSpec = parser.accepts("output").withRequiredArg().ofType(File.class);

		OptionSet options = parser.parse(Platform.getApplicationArgs());

		File outputDir = options.valueOf(outputDirSpec);
		outputDir.mkdirs();

		Map<String, Map<String, List<String>>> config;

		try (Reader in = new FileReader(options.valueOf(configSpec))) {
			Gson gson = new Gson();
			config = gson.fromJson(in, new TypeToken<Map<String, Map<String, List<String>>>>() {
			}.getType());
		}

		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		int counter=0;
		for (String projectName : config.keySet()) {

			IJavaProject project = JavaCore.create(workspaceRoot.getProject(projectName));

			for (String testClassName : config.get(projectName).keySet()) {

				IType testClass = project.findType(testClassName);

				for (String testMethodName : config.get(projectName).get(testClassName)) {

					IMethod testMethod = testClass.getMethod(testMethodName, new String[] {});
					System.out.println(testClassName);

					try {
						counter++;
						FactsFinder finder=FactsFinderFactory.create(testMethod);
						
						System.out.println(finder);
												
						TestNameTemplate template=finder.generateTemplate();
						
						System.out.println("Minimal Test Name:\t"+template.getMinimalTestName());
						System.out.println("Small Test Name:\t"+template.getSmallTestName());
						System.out.println("Medium Test Name:\t"+template.getMediumTestName());
						System.out.println("Full Test Name:\t\t"+template.getFullTestName());

						
						ActionGraph actionGraph=finder.getActionGraph();
						
						DOTExporter<ASTNode> exporter = new DOTExporter<>(actionGraph::nodeAttributes, actionGraph::edgeAttributes);

						String dotFileName = String.format("%s/%s.%s.dot", outputDir, testClassName, testMethodName);
						try (Writer writer = new FileWriter(dotFileName)) {
							exporter.export(writer, actionGraph);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					System.out.println("---------------------------------------------------");
				}
			}
		}

		System.out.println("Total:"+counter);
		// IType cls = context.testClass();
		//
		// ImmutableList<IJavaElement> scope = ImmutableList.<IJavaElement>
		// builder().add(cls.getFields())
		// .add(cls.getMethod("setUp", new String[] {}))
		// .add(cls.getMethod(options.valueOf(testName), new String[]
		// {})).build();
		//
		// IJavaSearchScope searchScope = SearchEngine
		// .createJavaSearchScope(scope.toArray(new
		// IJavaElement[scope.size()]));
		//
		// Searcher searcher = new Searcher(searchScope);
		//
		// CompilationUnit cu =
		// CompilationUnitCache.get(context.testClass().getTypeRoot())
		// .orElseThrow(RuntimeException::new);
		//
		// Set<IJavaElement> elements = ASTNodeUtils.find(context.testMethod(),
		// SimpleName.class)
		// .filter(name -> "params".equals(name.getIdentifier()))
		// .map(name ->
		// name.resolveBinding().getJavaElement()).distinct().collect(Collectors.toSet());
		//
		// for (IJavaElement element : elements) {
		//
		// System.out.println(element);
		//
		// System.out.println("DECLARATIONS");
		// for (SearchMatch match :
		// searcher.declarations(element).collect(Collectors.toSet())) {
		// ASTNode node = NodeFinder.perform(cu, match.getOffset(),
		// match.getLength());
		// ASTNode parent = node.getParent();
		// System.out.printf("\t%s [%s]\n", parent,
		// parent.getClass().getSimpleName());
		// }
		//
		// System.out.println("REFERENCES");
		// for (SearchMatch match :
		// searcher.references(element).collect(Collectors.toSet())) {
		// if (match instanceof LocalVariableReferenceMatch) {
		// LocalVariableReferenceMatch localVariableReferenceMatch =
		// (LocalVariableReferenceMatch) match;
		// ASTNode node = NodeFinder.perform(cu,
		// localVariableReferenceMatch.getOffset(),
		// localVariableReferenceMatch.getLength());
		// ASTNode parent = node.getParent();
		// System.out.printf("\t%s [%s] [%s]\n", parent,
		// parent.getClass().getSimpleName(),
		// localVariableReferenceMatch.isReadAccess() ? "read" : "write");
		// }
		// if (match instanceof FieldReferenceMatch) {
		// FieldReferenceMatch fieldReferenceMatch = (FieldReferenceMatch)
		// match;
		// ASTNode node = NodeFinder.perform(cu,
		// fieldReferenceMatch.getOffset(),
		// fieldReferenceMatch.getLength());
		// ASTNode parent = node.getParent();
		// System.out.printf("\t%s [%s] [%s]\n", parent,
		// parent.getClass().getSimpleName(),
		// fieldReferenceMatch.isReadAccess() ? "read" : "write");
		// }
		// }
		// }

		// Consumer<IMethodBinding> process = new Consumer<IMethodBinding>() {
		//
		// ReturnValueNamer returnValueNamer = new
		// ReturnValueNamer(context.searchScope());
		//
		// public void accept(IMethodBinding binding) {
		//
		// IMethod method = (IMethod) binding.getJavaElement();
		//
		// System.out.println(method);
		// System.out.println("\t@params: " + JavadocParamsFinder.find(method));
		// System.out.println("\t@return: " + JavadocReturnFinder.find(method));
		// System.out.println("\t" + returnValueNamer.findNames(method));
		// }
		// };
		//
		// for (IPackageFragment pkg : context.project().getPackageFragments())
		// {
		//
		// if (IPackageFragmentRoot.K_SOURCE != pkg.getKind()) {
		// continue;
		// }
		//
		// Arrays.stream(pkg.getCompilationUnits())
		// .map(CompilationUnitCache::get)
		// .filter(Optional::isPresent)
		// .map(Optional::get)
		// .flatMap(cu -> ASTNodeUtils.find(cu, MethodDeclaration.class))
		// .filter(decl -> "getWidth".equals(decl.getName().getIdentifier()))
		// .map(MethodDeclaration::resolveBinding)
		// .filter(binding -> null != binding)
		// .forEach(process);
		// }
		//
		// for (IPackageFragment pkg : context.project().getPackageFragments())
		// {
		//
		// if (IPackageFragmentRoot.K_SOURCE != pkg.getKind()) {
		// continue;
		// }
		//
		// Arrays.stream(pkg.getCompilationUnits())
		// .map(CompilationUnitCache::get)
		// .filter(Optional::isPresent)
		// .map(Optional::get)
		// .flatMap(cu -> ASTNodeUtils.find(cu, MethodDeclaration.class))
		// .flatMap(decl -> ASTNodeUtils.find(decl, MethodInvocation.class))
		// .map(MethodInvocation::resolveMethodBinding)
		// .filter(binding -> null != binding)
		// .distinct()
		// .forEach(process);
		// }

		return IApplication.EXIT_OK;

	}

	@Override
	public void stop() {
	}
}
