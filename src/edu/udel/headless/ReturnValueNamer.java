package edu.udel.headless;

import java.util.Set;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.internal.corext.callhierarchy.CallHierarchy;
import org.eclipse.jdt.internal.corext.callhierarchy.CallLocation;
import org.eclipse.jdt.internal.corext.callhierarchy.MethodCall;
import org.eclipse.jdt.internal.corext.callhierarchy.MethodWrapper;

import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multiset;

import edu.udel.headless.cache.CompilationUnitCache;

@SuppressWarnings({ "restriction" })
public class ReturnValueNamer {

	private final CallHierarchy callHierarchy;

	public ReturnValueNamer(final IJavaSearchScope searchScope) {
		this.callHierarchy = CallHierarchy.getDefault();
		this.callHierarchy.setSearchScope(searchScope);
	}

	public Multiset<String> findNames(final IMethod method) {

		ImmutableMultiset.Builder<String> builder = ImmutableMultiset.builder();

		for (ASTNode node : callsTo(method)) {

			ASTNode parent = node.getParent();

			if (ASTNode.CAST_EXPRESSION == parent.getNodeType()) {
				node = parent;
				parent = node.getParent();
			}

			switch (parent.getNodeType()) {
			case ASTNode.CLASS_INSTANCE_CREATION: {
				ClassInstanceCreation classInstanceCreation = (ClassInstanceCreation) parent;

				int index = classInstanceCreation.arguments().indexOf(node);

				if (index >= 0) {
					try {
						IMethodBinding binding = classInstanceCreation.resolveConstructorBinding();
						IMethod caller = (IMethod) binding.getJavaElement();
						String[] names = caller.getParameterNames();
						builder.add(names[index]);
					} catch (JavaModelException e) {
						e.printStackTrace();
					}
				}
				break;
			}
			case ASTNode.ASSIGNMENT: {
				Assignment assignent = (Assignment) parent;
				Expression lhs = assignent.getLeftHandSide();

				if (ASTNode.SIMPLE_NAME == lhs.getNodeType()) {
					SimpleName name = (SimpleName) lhs;
					builder.add(name.getIdentifier());
				}

				break;
			}
			case ASTNode.METHOD_INVOCATION: {
				MethodInvocation methodInvocation = (MethodInvocation) parent;
				int index = methodInvocation.arguments().indexOf(node);
				if (index >= 0) {
					try {
						IMethodBinding binding = methodInvocation.resolveMethodBinding();
						IMethod caller = (IMethod) binding.getJavaElement();
						String[] names = caller.getParameterNames();
						builder.add(names[index]);
					} catch (JavaModelException e) {
						e.printStackTrace();
					}
				}
				break;
			}
			case ASTNode.VARIABLE_DECLARATION_FRAGMENT: {
				VariableDeclarationFragment declaration = (VariableDeclarationFragment) parent;
				builder.add(declaration.getName().getIdentifier());
				break;
			}
			}
		}

		return builder.build();
	}

	private Set<MethodInvocation> callsTo(IMethod method) {

		ImmutableSet.Builder<MethodInvocation> builder = ImmutableSet.builder();

		MethodWrapper callee = callHierarchy.getCallerRoots(new IMember[] { method })[0];

		for (MethodWrapper wrapper : callee.getCalls(new NullProgressMonitor())) {

			MethodCall call = wrapper.getMethodCall();
			IMember caller = call.getMember();

			CompilationUnitCache.get(caller.getTypeRoot()).ifPresent(cu -> {
				for (CallLocation location : call.getCallLocations()) {
					int start = location.getStart();
					int length = location.getEnd() - start;

					ASTNode node = NodeFinder.perform(cu, start, length);

					if (node instanceof MethodInvocation) {
						builder.add((MethodInvocation) node);
					}
				}
			});
		}

		return builder.build();
	}
}
