package edu.udel.headless;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TagElement;

import edu.udel.headless.cache.JavadocCache;
import edu.udel.headless.util.ASTNodeUtils;
import edu.udel.headless.util.TagElementUtils;

public class JavadocParamsFinder {

	public static Optional<Map<SimpleName, String>> find(final IMethod method) {
		return JavadocCache.get(method).map(javadoc -> {
			return ASTNodeUtils.find(javadoc, TagElement.class)
					.filter(tag -> TagElement.TAG_PARAM.equals(tag.getTagName()))
					.filter(tag -> ASTNodeUtils.find(tag, SimpleName.class).count() > 0)					
					.collect(Collectors.toMap(tag -> ASTNodeUtils.find(tag, SimpleName.class).findFirst().get(), 
											  TagElementUtils::toString));
		});
	}
}
