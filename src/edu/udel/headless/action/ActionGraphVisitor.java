package edu.udel.headless.action;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

public class ActionGraphVisitor {
	
	public static List<ASTNode> visit(final ActionGraph graph,final int depth) {

			ActionGraphVisitor visitor = new ActionGraphVisitor(graph);
			return visitor.visit(depth);	
	}
	
	private final ActionGraph actionGraph;
	
	private ActionGraphVisitor(ActionGraph graph){
		this.actionGraph=graph;
	}
	
	private List<ASTNode> visit(final int depth){
		LinkedList<ASTNode> list=new LinkedList<ASTNode>();
		HashSet<ASTNode> visited=new HashSet<ASTNode>();
		HashSet<ASTNode> frontier=new HashSet<ASTNode>();
		ASTNode root = actionGraph.roots().findAny().orElseThrow(RuntimeException::new);
		frontier.add(root);	
		for(int i=0;i<=depth;i++){
									
			if(frontier.size()==0) break;
			
			visited.addAll(frontier);
			
			frontier = (HashSet<ASTNode>) frontier
					  .stream()
					  .flatMap(node->actionGraph.neighbors(node))
					  .distinct()
					  .filter(node->!visited.contains(node))					  
					  .collect(Collectors.toSet());
			
			list.addAll(frontier
					.stream()
					.filter(node->{
						if(node instanceof Assignment && ((Assignment)node).getRightHandSide() instanceof ClassInstanceCreation){							
							return false;
						}
						else return true;
					})
					.sorted(new ASTNodeComparator())
					.collect(Collectors.toList())
					);
		}
		
		return list;		
	}
	

}
