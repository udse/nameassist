package edu.udel.headless.action;

import java.util.List;
import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;

public class SimpleNameFinder extends ASTVisitor {

	private final Stream.Builder<SimpleName> builder;

	public SimpleNameFinder() {
		this.builder = Stream.builder();
	}

	public Stream<SimpleName> names() {
		return builder.build();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean visit(ClassInstanceCreation invocation) {

		Expression expression = invocation.getExpression();

		if (null != expression) {
			expression.accept(this);
		}

		for (Expression argument : (List<Expression>) invocation.arguments()) {
			argument.accept(this);
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean visit(ConstructorInvocation invocation) {

		for (Expression argument : (List<Expression>) invocation.arguments()) {
			argument.accept(this);
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean visit(MethodInvocation invocation) {

		Expression expression = invocation.getExpression();

		if (null != expression) {
			expression.accept(this);
		}

		for (Expression argument : (List<Expression>) invocation.arguments()) {
			argument.accept(this);
		}

		return false;
	}

	@Override
	public boolean visit(final SimpleName name) {

		builder.accept(name);

		return super.visit(name);
	}

	public static Stream<SimpleName> find(final ASTNode root) {				
		
		SimpleNameFinder finder = new SimpleNameFinder();
		root.accept(finder);
		return finder.names();
	}

}
