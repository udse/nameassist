package edu.udel.headless.action;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTMatcher;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import edu.udel.headless.cache.CompilationUnitCache;
import edu.udel.headless.cache.MethodDeclarationCache;
import edu.udel.headless.util.ASTNodeUtils;

public class ActionGraphBuilder {

	public static ActionGraph build(final IMethod testMethod,final Set<String> words) {
		try {
			ActionGraphBuilder builder = new ActionGraphBuilder(testMethod,words);
			return builder.build();			
		} catch (JavaModelException e) {
			throw new RuntimeException(e);
		}
	}

	private final IType testClass;
	private final IMethod testMethod;
	private Searcher searcher;
	private boolean includeSetUp;
	private final Set<SimpleName> actionObjects;
	private final CompilationUnit testClassAST;
	private final Set<String> words;

	private ActionGraphBuilder(final IMethod testMethod,final Set<String> words) throws JavaModelException {
		this.testMethod=testMethod;
		this.testClass = testMethod.getDeclaringType();

		this.testClassAST = CompilationUnitCache.get(testClass.getCompilationUnit())
				.orElseThrow(RuntimeException::new);		
		
		this.searcher=new Searcher(testMethod);
		this.includeSetUp=false;				
		this.actionObjects = ActionObjectFinder.find(testMethod);
		this.words=words;
	}	
	
	private void addAssertionEdge(ActionGraph actionGraph){
		ASTNode root = actionGraph.roots()
				.findAny()
				.orElseThrow(RuntimeException::new);
		
		System.out.println("Assertion Edge:");
		
		for (String word : words) {
			for (ASTNode node : actionGraph.nodes().collect(Collectors.toSet())) {

				if (node.equals(root))
					continue;

				if (node.toString().toLowerCase().contains(word)) {
					actionGraph.add(root, node, ActionGraph.Edge.ASSERTION);
					System.out.printf("\t[%s]:%s-->%s\n", word, root, node);
				}
			}
		}
		
		System.out.println("\n\n\n");
	}

	private ActionGraph build() {

		ActionGraph graph = new ActionGraph();

		Set<ASTNode> visitedNodes = Sets.newHashSet();
		Set<String> visitedNames = Sets.newHashSet();

		LinkedList<ASTNode> worklist = Lists.newLinkedList(actionObjects);

		while (!worklist.isEmpty()) {

			ASTNode current = worklist.poll();

			if (visitedNodes.contains(current)) {
				continue;
			}
			visitedNodes.add(current);
			graph.add(current);

			for (SimpleName name : SimpleNameFinder.find(current).collect(Collectors.toSet())) {

				if (visitedNames.contains(name.getIdentifier())) {
					continue;
				}
				visitedNames.add(name.getIdentifier());

				System.out.printf("\t%s\n", name);

				Optional<ASTNode> declaration = declaration(name);

				Set<ASTNode> uses = uses(name).stream()
						.filter(use -> !use.subtreeMatch(new ASTMatcher(), current))
						.filter(use -> !ASTNodeUtils.isGetter(use))
						.collect(Collectors.toSet());

				System.out.printf("\t\tuses: %s\n", uses);

				for (ASTNode use : uses) {
					// Add an edge to each use from current
					graph.add(current, use, ActionGraph.Edge.DEPENDENCY);

					System.out.printf("\t\t\t%s -> %s\n", current, use);

					worklist.add(use);
				}

				System.out.printf("\t\tdecl: %s\n", declaration);

				declaration(name)
						.filter(decl -> !decl.subtreeMatch(new ASTMatcher(), current))
						.ifPresent(decl -> {

							// add an edge from each use to declaration
							for (ASTNode use : uses) {
								graph.add(use, decl, ActionGraph.Edge.DEPENDENCY);

								System.out.printf("\t\t\t%s -> %s\n", use, decl);
							}

							// only add an edge from current to declaration if there are no uses
							// (if there are uses, we'll already have current -> use -> declaration)
							if (uses.isEmpty()) {
								graph.add(current, decl, ActionGraph.Edge.DEPENDENCY);

								System.out.printf("\t\t\t%s -> %s\n", current, decl);
							}

							worklist.add(decl);
						});
			}
		}
		
		if(graph.nodes().count()<2 &&!includeSetUp){
			this.searcher = new Searcher(testMethod,testClass.getMethod("setUp", new String[] {}));
			this.includeSetUp=true;
			return build();
		}
		else{
		 addAssertionEdge(graph);
		 return graph;
		}
	}

	private Optional<ASTNode> declaration(final SimpleName name) {

		IJavaElement element = name.resolveBinding().getJavaElement();

		return searcher.writes(element)
				.map(match -> NodeFinder.perform(testClassAST, match.getOffset(), match.getLength()))
				.map(ASTNode::getParent)
				.findFirst();
	}

	private static ASTNode findRelevantParent(final ASTNode node) {
		ASTNode parent = node.getParent();
		while(!(parent instanceof ExpressionStatement)
				&& !(parent instanceof MethodInvocation)
				&& !(parent instanceof Assignment)
				&& !(parent instanceof SuperMethodInvocation)) {
			parent = parent.getParent();
		}
		return parent;
	}
	
	private Set<ASTNode> uses(final SimpleName name) {

		IJavaElement element = name.resolveBinding().getJavaElement();

		return searcher.reads(element)
				.map(match -> NodeFinder.perform(testClassAST, match.getOffset(), match.getLength()))
				.map(ActionGraphBuilder::findRelevantParent)
				.collect(Collectors.toSet());
	}
}
