package edu.udel.headless.action;

import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;

import com.google.common.collect.ImmutableSet;

import edu.udel.headless.ClassUnderTestFinder;
import edu.udel.headless.cache.MethodDeclarationCache;
import edu.udel.headless.util.ASTNodeUtils;
import edu.udel.headless.util.StreamUtils;

public class ActionObjectFinder {

	public static Set<SimpleName> find(final IMethod testMethod) throws JavaModelException {

		IType classUnderTest = ClassUnderTestFinder.find(testMethod).orElseThrow(RuntimeException::new);

		Set<IType> classUnderTestSubtypes = ImmutableSet.<IType> builder()
				.add(classUnderTest)
				.add(classUnderTest.newTypeHierarchy(new NullProgressMonitor()).getAllSubtypes(classUnderTest))
				.build();

		MethodDeclaration testMethodAST = MethodDeclarationCache.get(testMethod)
				.orElseThrow(RuntimeException::new);

		return ASTNodeUtils.find(testMethodAST, SimpleName.class)
				.filter(StreamUtils.distinctByKey(SimpleName::getIdentifier))
				.filter(name -> name.resolveBinding() instanceof IVariableBinding)
				.filter(name -> {
					IType nameType = (IType) ((ITypeBinding) name.resolveTypeBinding()).getJavaElement();
					return classUnderTestSubtypes.contains(nameType);
				})
				.collect(Collectors.toSet());								
	}
}
