package edu.udel.headless.action;

import java.util.stream.Stream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.search.FieldReferenceMatch;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.LocalVariableReferenceMatch;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.core.search.SearchMatch;
import org.eclipse.jdt.core.search.SearchParticipant;
import org.eclipse.jdt.core.search.SearchPattern;
import org.eclipse.jdt.core.search.SearchRequestor;

public class Searcher {

	private final IJavaSearchScope searchScope;
	private final SearchEngine engine;

	public Searcher(final IJavaElement... elements) {
		this.engine = new SearchEngine();
		this.searchScope = SearchEngine.createJavaSearchScope(elements);
	}

	public Stream<SearchMatch> search(final IJavaElement element, final int limitTo) {
		Stream.Builder<SearchMatch> builder = Stream.builder();

		try {
			SearchPattern pattern = SearchPattern.createPattern(element, limitTo);

			engine.search(pattern,
					new SearchParticipant[] { SearchEngine.getDefaultSearchParticipant() },
					searchScope,
					new SearchRequestor() {
						public void acceptSearchMatch(SearchMatch match) {
							builder.add(match);
						}
					},
					null);
		} catch (CoreException e) {
		}

		return builder.build();
	}

	public Stream<SearchMatch> declarations(final IJavaElement element) {
		return search(element, IJavaSearchConstants.DECLARATIONS);
	}

	public Stream<SearchMatch> references(final IJavaElement element) {
		return search(element, IJavaSearchConstants.REFERENCES);
	}

	public Stream<SearchMatch> reads(final IJavaElement element) {
		return search(element, IJavaSearchConstants.REFERENCES)
				.filter(match -> {
					if (match instanceof LocalVariableReferenceMatch) {
						return ((LocalVariableReferenceMatch) match).isReadAccess();
					} else if (match instanceof FieldReferenceMatch) {
						return ((FieldReferenceMatch) match).isReadAccess();
					}
					return false;
				});
	}

	public Stream<SearchMatch> writes(final IJavaElement element) {
		return search(element, IJavaSearchConstants.REFERENCES)
				.filter(match -> {
					if (match instanceof LocalVariableReferenceMatch) {
						return ((LocalVariableReferenceMatch) match).isWriteAccess();
					} else if (match instanceof FieldReferenceMatch) {
						return ((FieldReferenceMatch) match).isWriteAccess();
					}
					return false;
				});
	}
}
