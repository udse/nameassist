package edu.udel.headless.action;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.ASTNode;

import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.MutableClassToInstanceMap;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import com.google.common.html.HtmlEscapers;

import edu.udel.graph.AnnotatedGraph;
import edu.udel.graph.DirectedGraph;

public class ActionGraph implements DirectedGraph<ASTNode>, AnnotatedGraph<ASTNode> {

	public static enum Edge {
		DEPENDENCY, ASSERTION,
	}

	private final Set<ASTNode> nodes;
	private final Table<ASTNode, ASTNode, ClassToInstanceMap<Object>> edges;

	public ActionGraph() {
		this.nodes = Sets.newHashSet();
		this.edges = HashBasedTable.create();
	}

	public void add(final ASTNode node) {
		nodes.add(node);
	}

	public void add(ASTNode source, ASTNode target) {
		add(source);
		add(target);
		if (!edges.contains(source, target)) {
			edges.put(source, target, MutableClassToInstanceMap.create());
		}
	}

	public <E> void add(ASTNode source, ASTNode target, Class<E> cls, E annotation) {
		add(source, target);
		edges.get(source, target).put(cls, annotation);
	}

	public <E> void add(ASTNode source, ASTNode target, E annotation) {
		add(source, target);
		edges.get(source, target).put(annotation.getClass(), annotation);
	}

	public <E> void add(ASTNode source, ASTNode target, Map<Class<? extends Object>, Object> annotations) {
		add(source, target);
		edges.get(source, target).putAll(annotations);
	}

	@Override
	public Stream<ASTNode> nodes() {
		return nodes.stream();
	}

	@Override
	public Stream<ASTNode> successors(final ASTNode node) {
		return edges.row(node).keySet().stream();
	}

	@Override
	public Stream<ASTNode> predecessors(final ASTNode node) {
		return edges.column(node).keySet().stream();
	}

	@Override
	public Collection<Object> annotations(ASTNode n1, ASTNode n2) {
		return edges.get(n1, n2).values();
	}

	@Override
	public <A> Optional<A> annotation(ASTNode n1, ASTNode n2, Class<A> cls) {
		return Optional.ofNullable(edges.get(n1, n2).getInstance(cls));
	}

	public Map<String, String> nodeAttributes(final ASTNode node) {
		return ImmutableMap.of(
				"label", String.format("\"%s|%s\"", HtmlEscapers.htmlEscaper().escape(node.toString()), "a"),
				"shape", "Mrecord");
	}

	public Map<String, String> edgeAttributes(final ASTNode source, final ASTNode target) {
		return ImmutableMap.of(
				"style", annotation(source, target, ActionGraph.Edge.class)
						.filter(ActionGraph.Edge.ASSERTION::equals)
						.map(type -> "dashed")
						.orElse("solid"));
	}
	
}