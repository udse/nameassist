package edu.udel.headless.action;

import java.util.Comparator;

import org.eclipse.jdt.core.dom.ASTNode;

public class ASTNodeComparator implements Comparator<ASTNode> {

	@Override
	public int compare(ASTNode node1, ASTNode node2) {
		return node1.getStartPosition()>node2.getStartPosition()?-1:1;
		
	}



}
