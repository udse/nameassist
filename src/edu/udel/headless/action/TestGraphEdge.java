package edu.udel.headless.action;

public class TestGraphEdge {

	public enum EdgeType{
		ACTION_DEPENDENCY_EDGE,
		ASSERTION_EDGE	
	}
	
	private EdgeType edgeType;

	public TestGraphEdge(EdgeType type) {
		this.edgeType = type;
	}

	public String toDOT() {
		return String.format("[format = %s]", (edgeType == EdgeType.ACTION_DEPENDENCY_EDGE) ? "solid" : "dashed");
	}
	
	public static TestGraphEdge actionEdge() {
		return new TestGraphEdge(EdgeType.ACTION_DEPENDENCY_EDGE);
	}

	public static TestGraphEdge assertionEdge() {
		return new TestGraphEdge(EdgeType.ASSERTION_EDGE);
	}

	
}
