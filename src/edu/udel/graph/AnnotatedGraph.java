package edu.udel.graph;

import java.util.Collection;
import java.util.Optional;

public interface AnnotatedGraph<V> extends Graph<V> {
	public Collection<Object> annotations(V n1, V n2);
	public <A> Optional<A> annotation(V n1, V n2, Class<A> cls);
}
