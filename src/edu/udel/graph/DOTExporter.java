package edu.udel.graph;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DOTExporter<V> {

	private static final Function<Map<String, String>, String> attributesFormatter = m -> m.entrySet().stream()
			.map(e -> String.format("%s = %s", e.getKey(), e.getValue()))
			.collect(Collectors.joining(", ", "[", "]"));

	private final Function<V, String> nodeIDProvider;
	private final Function<V, String> nodeAttributeProvider;
	private final BiFunction<V, V, String> edgeAttributeProvider;

	public DOTExporter(final Function<V, String> nodeIDProvider,
			final Function<V, Map<String, String>> nodeAttributeProvider,
			final BiFunction<V, V, Map<String, String>> edgeAttributeProvider) {
		this.nodeIDProvider = nodeIDProvider;
		this.nodeAttributeProvider = nodeAttributeProvider.andThen(attributesFormatter);
		this.edgeAttributeProvider = edgeAttributeProvider.andThen(attributesFormatter);
	}

	public DOTExporter(final Function<V, Map<String, String>> nodeAttributeProvider,
			final BiFunction<V, V, Map<String, String>> edgeAttributeProvider) {
		this(node -> String.format("\"%x\"", System.identityHashCode(node)),
				nodeAttributeProvider,
				edgeAttributeProvider);
	}

	public void export(Writer writer, Graph<V> graph) {
		PrintWriter out = new PrintWriter(writer);

		out.println("graph {");

		graph.nodes().forEach(node -> {
			out.format("\t%s %s;\n",
					nodeIDProvider.apply(node),
					nodeAttributeProvider.apply(node));
		});

		graph.nodes().forEach(node -> {
			graph.neighbors(node).forEach(neighbor -> {
				out.format("\t%s -- %s %s;\n",
						nodeIDProvider.apply(node),
						nodeIDProvider.apply(neighbor),
						edgeAttributeProvider.apply(node, neighbor));
			});
		});

		out.println("}");
		out.flush();
	}

	public void export(Writer writer, DirectedGraph<V> graph) {
		PrintWriter out = new PrintWriter(writer);

		out.println("digraph {");

		graph.nodes().forEach(node -> {
			out.format("\t%s %s;\n",
					nodeIDProvider.apply(node),
					nodeAttributeProvider.apply(node));
		});

		graph.nodes().forEach(node -> {
			graph.neighbors(node).forEach(neighbor -> {
				out.format("\t%s -> %s %s;\n",
						nodeIDProvider.apply(node),
						nodeIDProvider.apply(neighbor),
						edgeAttributeProvider.apply(node, neighbor));
			});
		});

		out.println("}");
		out.flush();
	}

}
