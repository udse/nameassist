package edu.udel.graph;

import java.util.stream.Stream;

public interface Graph<V> {
	public Stream<V> nodes();
	public Stream<V> neighbors(final V node);
}
