package edu.udel.graph;

import java.util.stream.Stream;

public interface DirectedGraph<V> extends Graph<V> {
	
	public Stream<V> successors(final V node);
	public Stream<V> predecessors(final V node);
	
	@Override
	public default Stream<V> neighbors(final V node) {
		return successors(node);
	}
	
	public default Stream<V> roots() {
		return nodes().filter(node -> 0 == predecessors(node).count());
	}
}
