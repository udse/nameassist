package edu.udel.graph.traversal;

import java.util.function.Consumer;

import edu.udel.graph.Graph;

public class BreadthFirstIterator<V> extends AbstractGraphIterator<V> {

	public BreadthFirstIterator(final Graph<V> graph, final V root) {
		super(graph, root);
	}

	protected Consumer<? super V> action() {
		return frontier::addLast;
	}
}
