package edu.udel.graph.traversal;

import java.util.function.Consumer;

import edu.udel.graph.Graph;

public class DepthFirstIterator<V> extends AbstractGraphIterator<V> {

	public DepthFirstIterator(final Graph<V> graph, final V root) {
		super(graph, root);
	}

	@Override
	protected Consumer<? super V> action() {
		return frontier::addFirst;
	}
				
}
