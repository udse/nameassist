package edu.udel.graph.traversal;

import java.util.Deque;
import java.util.Set;
import java.util.function.Consumer;

import com.google.common.collect.AbstractIterator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;

import edu.udel.graph.Graph;

public abstract class AbstractGraphIterator<V> extends AbstractIterator<V> {

	protected final Graph<V> graph;
	protected final Deque<V> frontier;
	protected final Set<V> visited;
	
	protected AbstractGraphIterator(final Graph<V> graph, final V root) {
		this.graph = graph;
		this.frontier = Queues.newArrayDeque(ImmutableList.of(root));
		this.visited = Sets.newHashSet();
	}

	protected abstract Consumer<? super V> action();
	
	@Override
	protected V computeNext() {		
		if (frontier.isEmpty()) {
			return endOfData();
		}
		
		V node = frontier.removeFirst();

		visited.add(node);

		graph.neighbors(node)
				.filter(neighbor -> !visited.contains(neighbor))
				.forEach(action());

		return node;
	}
}
